package main

import (
	"entrega-infraestrutura/internal/apiError"
	"entrega-infraestrutura/internal/bike"
	bikeForms "entrega-infraestrutura/internal/bike/forms"
	"entrega-infraestrutura/internal/lock"
	lockForms "entrega-infraestrutura/internal/lock/forms"
	"entrega-infraestrutura/internal/totem"
	totemForms "entrega-infraestrutura/internal/totem/forms"
	"entrega-infraestrutura/internal/utils"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"syscall"
)

const PORT string = "3001"

func main() {
	mux := http.NewServeMux()

	mux.HandleFunc("GET /", func(w http.ResponseWriter, r *http.Request) {
		body := make(map[string]any)
		body["status"] = "microsservico equipamento esta online!"

		utils.WriteJson(w, 200, body)
	})

	mux.HandleFunc("GET /restaurarDados", func(w http.ResponseWriter, r *http.Request) {
		body := make(map[string]string)
		body["status"] = "dados resetados"

		utils.WriteJson(w, 200, body)
		go RestartApp()
	})

	mux.HandleFunc("GET /restaurarBanco", func(w http.ResponseWriter, r *http.Request) {
		body := make(map[string]string)
		body["status"] = "dados resetados"

		utils.WriteJson(w, 200, body)
		go RestartApp()
	})

	// Creating routes from each Domain
	totemController := totem.TotemController{
		Mux: mux,

		DefaultHandlerCreator:        utils.MakeHTTPHandlerFunc,
		HandlerWithValidationCreator: utils.MakeHTTPHandlerFuncWithValidation,

		TotemDomain: totem.NewInMemory(),

		IdValidator: utils.ValidateId,
		JsonWriter:  utils.WriteJson,
		BodyDecoder: utils.DecodeBody,

		TotemCreationValidator: totemForms.ValidateTotemCreationMock,

		InternalServerErrorCreator: apiError.InternalServerErrorApiError,
		ValidationErrorsCreator:    apiError.NewWithValidationErrors,
	}
	totemController.InitializeRoutes()

	lockController := lock.LockController{
		Mux: mux,

		DefaultHandlerCreator:        utils.MakeHTTPHandlerFunc,
		HandlerWithValidationCreator: utils.MakeHTTPHandlerFuncWithValidation,

		LockDomain: lock.NewInMemory(),

		IdValidator: utils.ValidateId,
		JsonWriter:  utils.WriteJson,
		BodyDecoder: utils.DecodeBody,

		LockCreationValidation:     lockForms.ValidateLockCreation,
		LockIntegrationValidation:  lockForms.ValidateLockIntegration,
		LockReleaseValidation:      lockForms.ValidateLockRelease,
		LockStatusUpdateValidation: lockForms.ValidateLockStatus,
		BikeAssociationValidation:  lockForms.ValidateBikeAssociation,

		InternalServerErrorCreator: apiError.InternalServerErrorApiError,
		ValidationErrorsCreator:    apiError.NewWithValidationErrors,
	}
	lockController.InitializeRoutes()

	bikeController := bike.BikeController{
		Mux: mux,

		DefaultHandlerCreator:        utils.MakeHTTPHandlerFunc,
		HandlerWithValidationCreator: utils.MakeHTTPHandlerFuncWithValidation,

		BikeDomain: bike.NewInMemory(),

		IdValidator: utils.ValidateId,
		JsonWriter:  utils.WriteJson,
		BodyDecoder: utils.DecodeBody,

		BikeCreationValidator:     bikeForms.ValidateBikeCreation,
		BikeIntegrationValidation: bikeForms.ValidateBikeIntegration,
		BikeReleaseValidation:     bikeForms.ValidateBikeRelease,

		InternalServerErrorCreator: apiError.InternalServerErrorApiError,
		ValidationErrorsCreator:    apiError.NewWithValidationErrors,
	}
	bikeController.InitializeRoutes()

	fmt.Println("Server started on http://localhost:" + PORT)

	err := http.ListenAndServe(":"+PORT, mux)
	if err != nil {
		panic(err)
	}
}

func RestartApp() error {
	self, err := os.Executable()
	if err != nil {
		return err
	}
	args := os.Args
	env := os.Environ()
	// Windows does not support exec syscall.
	if runtime.GOOS == "windows" {
		cmd := exec.Command(self, args[1:]...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		cmd.Stdin = os.Stdin
		cmd.Env = env
		err := cmd.Run()
		if err == nil {
			os.Exit(0)
		}
		return err
	}
	return syscall.Exec(self, args, env)
}

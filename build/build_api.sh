# /bin/bash


echo "Building the project..."
mkdir -p mybinaries
go build -o mybinaries ./...

echo "stopping API process if exists"

ssh $SERVER_USER@$SERVER_IP << EOF
  PID=\$(lsof -t -i:3001)
  
  # If a PID is found, kill the process
  if [ -n "\$PID" ]; then
    kill \$PID
    echo "Process running on port 3001 stopped."
  else
    echo "No process found running on port 3001."
  fi
EOF


echo "Deploying new binaries..."
rsync -a mybinaries/ $SERVER_USER@$SERVER_IP:/home/$SERVER_USER/mybinaries

echo "Running the server..."
ssh $SERVER_USER@$SERVER_IP "cd /home/$SERVER_USER && screen -dmS api ./mybinaries/api" 

echo "Done!"

package validations

import "reflect"

func isString(val any, fieldName string) error {
	if reflect.TypeOf(val).Kind() != reflect.String {
		return getValidationError(fieldName, "is not a string")
	}

	return nil
}

func IsEmptyString(val any, fieldName string) error {
	err := isString(val, fieldName)
	if err != nil {
		return err
	}

	if val == "" {
		return getValidationError(fieldName, "string is empty")
	}

	return nil
}

func isNumber(val any, fieldName string) error {
	if reflect.TypeOf(val).Kind() != reflect.Int {
		return getValidationError(fieldName, "is not a number")
	}

	return nil
}

func MustBeGreaterThanZero(val any, fieldName string) error {
	err := isNumber(val, fieldName)
	if err != nil {
		return err
	}

	if reflect.ValueOf(val).Int() <= 0 {
		return getValidationError(fieldName, "number must be greater than zero")
	}

	return nil
}

func NewLockStatusCreation(val any, fieldName string) error {
	err := isString(val, fieldName)
	if err != nil {
		return err
	}

	if val != "NOVA" {
		return getValidationError(fieldName, "invalid lock status")
	}

	return nil
}

func NewBikeStatusCreation(val any, fieldName string) error {
	err := isString(val, fieldName)
	if err != nil {
		return err
	}

	if val != "NOVA" {
		return getValidationError(fieldName, "invalid bike status")
	}

	return nil
}

func LockReleaseStatus(val any, fieldName string) error {
	err := isString(val, fieldName)
	if err != nil {
		return err
	}

	if val != "APOSENTADA" && val != "EM_REPARO" {
		return getValidationError(fieldName, "invalid lock status")
	}

	return nil
}

package validations

import "errors"

func mockValidationFuncReturningErr(val any, fieldName string) error {
	return errors.New("error returned in mock validator")
}

func mockValidationFuncReturningNil(val any, fieldName string) error {
	return nil
}

package validations

import (
	"errors"
	"fmt"
)

type ValidationFunc func(val any, fieldName string) error

type FieldValidation struct {
	FieldName   string
	FieldValue  any
	Validations []ValidationFunc
}

func Validator(fieldValidations []FieldValidation) []error {
	validationErrors := []error{}

	for _, fieldValidation := range fieldValidations {
		for _, validationFunc := range fieldValidation.Validations {
			err := validationFunc(fieldValidation.FieldValue, fieldValidation.FieldName)
			if err == nil {
				continue
			}
			validationErrors = append(validationErrors, err)
		}
	}

	return validationErrors
}

func getValidationError(fieldName string, message string) error {
	errorString := fmt.Sprintf("%s: %s", fieldName, message)
	return errors.New(errorString)
}

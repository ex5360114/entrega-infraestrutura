package validations

import (
	"errors"
	"testing"
)

func TestIsString(t *testing.T) {
	tests := []struct {
		input          any
		fieldName      string
		expectedOutput error
	}{
		{1, "exampleField", errors.New("exampleField: is not a string")},
		{5, "exampleField", errors.New("exampleField: is not a string")},
		{true, "exampleField", errors.New("exampleField: is not a string")},
	}

	for _, test := range tests {
		output := isString(test.input, test.fieldName)
		if output != nil && test.expectedOutput != nil {
			if output.Error() != test.expectedOutput.Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput, output)
			}
		}

	}
}

func TestIsNumber(t *testing.T) {
	tests := []struct {
		input          any
		fieldName      string
		expectedOutput error
	}{
		{"", "exampleField", errors.New("exampleField: is not a number")},
		{5, "exampleField", nil},
		{true, "exampleField", errors.New("exampleField: is not a number")},
	}

	for _, test := range tests {
		output := isNumber(test.input, test.fieldName)
		if output != nil && test.expectedOutput != nil {
			if output.Error() != test.expectedOutput.Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput, output)
			}
		}
	}
}

func TestIsEmptyString(t *testing.T) {
	tests := []struct {
		input          any
		fieldName      string
		expectedOutput error
	}{
		{"", "exampleField", errors.New("exampleField: string is empty")},
		{"example", "exampleField", nil},
		{4, "exampleField", errors.New("exampleField: is not a string")},
	}

	for _, test := range tests {
		output := IsEmptyString(test.input, test.fieldName)
		if output != nil && test.expectedOutput != nil {
			if output.Error() != test.expectedOutput.Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput, output)
			}
		}
	}
}

func TestNewLockStatusCreation(t *testing.T) {
	tests := []struct {
		input          any
		fieldName      string
		expectedOutput error
	}{
		{"", "status", errors.New("status: invalid lock status")},
		{"example", "status", errors.New("status: invalid lock status")},
		{"NOVA", "status", nil},
		{4, "status", errors.New("status: is not a string")},
	}

	for _, test := range tests {
		output := NewLockStatusCreation(test.input, test.fieldName)
		if output != nil && test.expectedOutput != nil {
			if output.Error() != test.expectedOutput.Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput, output)
			}
		}
	}
}

func TestNewBikeStatusCreation(t *testing.T) {
	tests := []struct {
		input          any
		fieldName      string
		expectedOutput error
	}{
		{"", "status", errors.New("status: invalid bike status")},
		{"example", "status", errors.New("status: invalid bike status")},
		{"NOVA", "status", nil},
		{4, "status", errors.New("status: is not a string")},
	}

	for _, test := range tests {
		output := NewBikeStatusCreation(test.input, test.fieldName)
		if output != nil && test.expectedOutput != nil {
			if output.Error() != test.expectedOutput.Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput, output)
			}
		}
	}
}

func TestMustBeGreaterThanZero(t *testing.T) {
	tests := []struct {
		input          any
		fieldName      string
		expectedOutput error
	}{
		{0, "exampleField", errors.New("exampleField: number must be greater than zero")},
		{-1, "exampleField", errors.New("exampleField: number must be greater than zero")},
		{1, "exampleField", nil},
		{"teste com string", "exampleField", errors.New("exampleField: is not a number")},
	}

	for _, test := range tests {
		output := MustBeGreaterThanZero(test.input, test.fieldName)
		if output != nil && test.expectedOutput != nil {
			if output.Error() != test.expectedOutput.Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput, output)
			}
		}
	}
}

func TestLockReleaseStatus(t *testing.T) {
	tests := []struct {
		input          any
		fieldName      string
		expectedOutput error
	}{
		{"", "status", errors.New("status: invalid lock status")},
		{"example", "status", errors.New("status: invalid lock status")},
		{"APOSENTADA", "status", nil},
		{"EM_REPARO", "status", nil},
		{4, "status", errors.New("status: is not a string")},
	}

	for _, test := range tests {
		output := LockReleaseStatus(test.input, test.fieldName)
		if output != nil && test.expectedOutput != nil {
			if output.Error() != test.expectedOutput.Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput, output)
			}
		}
	}
}

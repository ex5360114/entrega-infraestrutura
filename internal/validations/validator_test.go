package validations

import (
	"errors"
	"testing"
)

func TestValidatorWithMockError(t *testing.T) {
	// Create a slice of FieldValidation
	fieldValidations := []FieldValidation{
		{
			FieldName:   "exampleField",
			FieldValue:  "example",
			Validations: []ValidationFunc{mockValidationFuncReturningErr},
		},
	}

	// Call the Validator function
	validationErrors := Validator(fieldValidations)

	// Check if the validationErrors slice is empty
	if len(validationErrors) == 0 {
		t.Errorf("expected validationErrors to not be empty, but got none")
	}
}

func TestValidatorWithMockNil(t *testing.T) {
	// Create a slice of FieldValidation
	fieldValidations := []FieldValidation{
		{
			FieldName:   "exampleField",
			FieldValue:  "example",
			Validations: []ValidationFunc{mockValidationFuncReturningNil},
		},
	}

	// Call the Validator function
	validationErrors := Validator(fieldValidations)

	// Check if the validationErrors slice is empty
	if len(validationErrors) != 0 {
		t.Errorf("expected validationErrors to be empty, but got one")
	}
}

func TestValidatorOnValidValue(t *testing.T) {
	// Create a slice of FieldValidation
	fieldValidations := []FieldValidation{
		{
			FieldName:   "exampleField",
			FieldValue:  "example",
			Validations: []ValidationFunc{IsEmptyString},
		},
	}

	// Call the Validator function
	validationErrors := Validator(fieldValidations)

	// Check if the validationErrors slice is empty
	if len(validationErrors) != 0 {
		t.Errorf("Expected validationErrors to be empty, but got %v", validationErrors)
	}
}

func TestValidatorOnInValidValue(t *testing.T) {
	// Create a slice of FieldValidation
	fieldValidations := []FieldValidation{
		{
			FieldName:   "exampleField",
			FieldValue:  "",
			Validations: []ValidationFunc{IsEmptyString},
		},
	}

	validationErrors := Validator(fieldValidations)

	if len(validationErrors) != 1 {
		t.Errorf("Expected validationErrors to be have an error")
	}
}

func TestGetValidationError(t *testing.T) {
	fieldName := "exampleField"
	message := "example message"
	expectedOutput := errors.New("exampleField: example message")
	output := getValidationError(fieldName, message)

	if output.Error() != expectedOutput.Error() {
		t.Errorf("Expected error to be '%v', but got '%v'", expectedOutput, output)
	}
}

package utils

import (
	"encoding/json"
	"entrega-infraestrutura/internal/apiError"
	"fmt"
	"net/http"
	"strconv"
)

// Golang's http.HandlerFunc type describes a function that does not return any errors
// However, we want to be able to write handlers that are expected to return an error, so we can return these errors appropriatelly to the HTTP responses.
// Here, we are creating a wrapper around Golang's http.HandlerFunc that is capable of returning any error in a more programatic way.

type ApiFunc func(http.ResponseWriter, *http.Request) *apiError.ApiError

func MakeHTTPHandlerFunc(f ApiFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := f(w, r); err != nil {
			errJson, marshallingErr := json.Marshal(*err)
			if marshallingErr != nil {
				fmt.Println("Error marshalling error: ", marshallingErr)
			}
			fmt.Println("Sending Error: ", string(errJson))
			status, _ := strconv.Atoi(err.Status)
			WriteJson(w, status, err)
		}
	}
}

func MakeHTTPHandlerFuncMock(f ApiFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		f(w, r)
	}
}

type ValidatedApiFunc func(http.ResponseWriter, *http.Request) []apiError.ApiError

func MakeHTTPHandlerFuncWithValidation(f ValidatedApiFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		errs := f(w, r)

		// If no errors are returned, a 200 status code is written inside the ApiFunc and no error treatment is needed.
		if len(errs) == 0 {
			return
		}

		status, _ := strconv.Atoi(errs[0].Status)

		// If only one error is returned, this error may be due to a domain error or an internal server error, not necessarily an validation error.
		// If the status is not 422, we should return the error as a struct.
		// If the status is 422, we should return the error as an array of structs.
		if len(errs) == 1 {
			if status != http.StatusUnprocessableEntity {
				WriteJson(w, status, errs[0])
				return
			}
		}

		// If more than one error is returned, we should return the errors as an array of structs.
		WriteJson(w, status, errs)
	}
}

func MakeHTTPHandlerWithValidationFuncMock(f ValidatedApiFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		f(w, r)
	}
}

func WriteJson(w http.ResponseWriter, status int, v any) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	return json.NewEncoder(w).Encode(v)
}

func ValidateId(id string) (int, *apiError.ApiError) {
	if id == "" {
		return 0, apiError.InvalidIdApiError()
	}

	parsed, err := strconv.Atoi(id)

	if err != nil {
		return 0, apiError.InvalidIdApiError()
	}
	return parsed, nil
}

func MockValidateId(id string) (int, *apiError.ApiError) {
	return 1, nil
}

func DecodeBody(r http.Request, bodyStorage interface{}) error {
	err := json.NewDecoder(r.Body).Decode(bodyStorage)
	if err != nil {
		return err
	}
	return nil
}

package utils

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"entrega-infraestrutura/internal/apiError"
)

func TestValidateId(t *testing.T) {
	tests := []struct {
		input          string
		expectedOutput int
		expectedError  *apiError.ApiError
	}{
		{"123", 123, nil},
		{"", 0, apiError.InvalidIdApiError()},
		{"abc", 0, apiError.InvalidIdApiError()},
	}

	for _, test := range tests {
		output, err := ValidateId(test.input)
		if output != test.expectedOutput {
			t.Errorf("Expected output %d, got %d", test.expectedOutput, output)
		}
		if (err == nil && test.expectedError != nil) || (err != nil && test.expectedError == nil) {
			t.Errorf("Expected error %v, got %v", test.expectedError, err)
		} else if err != nil && test.expectedError != nil && err.Message != test.expectedError.Message {
			t.Errorf("Expected error message %s, got %s", test.expectedError.Message, err.Message)
		}
	}
}

func TestMakeHTTPHandlerFunc(t *testing.T) {
	handler := MakeHTTPHandlerFunc(func(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
		return apiError.RouteNotFoundApiError()
	})

	req, err := http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	expected := `{"mensagem":"route not found","codigo":"404"}`

	if strings.TrimRight(rr.Body.String(), "\n") != expected {
		t.Errorf("handler returned unexpected body: got %s want %s", rr.Body.String(), expected)
	}
}

func TestMakeHTTPHandlerFuncWithValidation(t *testing.T) {
	handler := MakeHTTPHandlerFuncWithValidation(func(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
		errorsToReturn := make([]error, 0)
		errorsToReturn = append(errorsToReturn, errors.New("Testing first validation"))
		errorsToReturn = append(errorsToReturn, errors.New("Testing second validation"))
		return apiError.NewWithValidationErrors(errorsToReturn)
	})

	req, err := http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	expected := `[{"mensagem":"Testing first validation","codigo":"422"},{"mensagem":"Testing second validation","codigo":"422"}]`

	if strings.TrimRight(rr.Body.String(), "\n") != expected {
		t.Errorf("handler returned unexpected body: got '%s' want '%s'", rr.Body.String(), expected)
	}

}

func TestMakeHTTPHandlerFuncWithValidationOtherErrorsCase(t *testing.T) {
	handler := MakeHTTPHandlerFuncWithValidation(func(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
		errorsToReturn := make([]apiError.ApiError, 0)
		errorsToReturn = append(errorsToReturn, *apiError.InternalServerErrorApiError(errors.New("testando outro erro")))
		return errorsToReturn
	})

	req, err := http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	expected := `{"mensagem":"testando outro erro","codigo":"500"}`

	if strings.TrimRight(rr.Body.String(), "\n") != expected {
		t.Errorf("handler returned unexpected body: got '%s' want '%s'", rr.Body.String(), expected)
	}
}

func TestMakeHTTPHandleFuncMock(t *testing.T) {
	MakeHTTPHandlerFuncMock(func(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
		return nil
	})
}

func TestMakeHTTPHandleFuncWithValidationMock(t *testing.T) {
	MakeHTTPHandlerWithValidationFuncMock(func(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
		return nil
	})
}
func TestMakeHTTPHandlerFuncWithValidationReturningOkResponse(t *testing.T) {
	handler := MakeHTTPHandlerFuncWithValidation(func(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
		WriteJson(w, http.StatusOK, map[string]string{"mensagem": "ok"})
		return nil
	})

	req, err := http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	expected := `{"mensagem":"ok"}`

	if strings.TrimRight(rr.Body.String(), "\n") != expected {
		t.Errorf("handler returned unexpected body: got '%s' want '%s'", rr.Body.String(), expected)
	}

}

func TestWriteJson(t *testing.T) {
	rr := httptest.NewRecorder()
	data := map[string]string{"hello": "world"}
	err := WriteJson(rr, http.StatusOK, data)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	contentType := rr.Result().Header.Get("Content-Type")
	if contentType != "application/json" {
		t.Errorf("handler returned wrong content-type header: got %v want %v", contentType, "application/json")
	}

	expected, _ := json.Marshal(data)
	if strings.TrimRight(rr.Body.String(), "\n") != strings.TrimRight(string(expected), "\n") {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), string(expected))
	}
}

func TestDecodeBody(t *testing.T) {
	type TestBody struct {
		Field string `json:"field"`
	}

	validBody := TestBody{Field: "value"}
	validBodyJson, _ := json.Marshal(validBody)

	tests := []struct {
		body          []byte
		expectedError error
		expectedValue TestBody
	}{
		{validBodyJson, nil, validBody},
		{[]byte(`{invalid json}`), errors.New("invalid character 'i' looking for beginning of object key string"), TestBody{}},
	}

	for _, test := range tests {
		req, err := http.NewRequest("POST", "/test", bytes.NewBuffer(test.body))
		if err != nil {
			t.Fatal(err)
		}

		var result TestBody
		err = DecodeBody(*req, &result)

		if (err == nil && test.expectedError != nil) || (err != nil && test.expectedError == nil) {
			t.Errorf("Expected error %v, got %v", test.expectedError, err)
		} else if err != nil && test.expectedError != nil && err.Error() != test.expectedError.Error() {
			t.Errorf("Expected error message %s, got %s", test.expectedError.Error(), err.Error())
		}

		if err == nil && result != test.expectedValue {
			t.Errorf("Expected value %v, got %v", test.expectedValue, result)
		}
	}
}

package bike

import (
	"entrega-infraestrutura/internal/apiError"
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
)

type mockBikeDomain struct{}

// IncludeBikeOnLock implements BikeDomain.
func (t *mockBikeDomain) IncludeBikeOnLock(data bikeAdapter.IntegrateBikeBaseData) *apiError.ApiError {
	return nil
}

// ReleaseBikeFromLock implements BikeDomain.
func (t *mockBikeDomain) ReleaseBikeFromLock(data bikeAdapter.ReleaseBikeBaseData) *apiError.ApiError {
	return nil
}

func (t *mockBikeDomain) Insert(bikeData bikeAdapter.BikeBaseData) (bikeAdapter.Bike, *apiError.ApiError) {
	return bikeAdapter.Bike{ID: 1, BikeBaseData: &bikeData}, nil
}

func (t *mockBikeDomain) Get() ([]bikeAdapter.Bike, *apiError.ApiError) {
	return make([]bikeAdapter.Bike, 0), nil
}

func (t *mockBikeDomain) GetByID(id int) (*bikeAdapter.Bike, *apiError.ApiError) {
	return &bikeAdapter.Bike{ID: 1, BikeBaseData: &bikeAdapter.BikeBaseData{
		Model:  "Modelo mockado",
		Year:   "2024",
		Number: 1,

		Status: "NOVA",
		Brand:  "marca mockada",
	}}, nil
}

func (t *mockBikeDomain) Update(id int, bikeData bikeAdapter.BikeBaseData) (bikeAdapter.Bike, *apiError.ApiError) {
	return bikeAdapter.Bike{ID: 1, BikeBaseData: &bikeData}, nil
}

func (t *mockBikeDomain) Delete(id int) *apiError.ApiError {
	return nil
}

func (l *mockBikeDomain) IncludeBikeOnTotem(data bikeAdapter.IntegrateBikeBaseData) *apiError.ApiError {
	return nil
}

func (l *mockBikeDomain) ReleaseBikeFromTotem(data bikeAdapter.ReleaseBikeBaseData) *apiError.ApiError {
	return nil
}

func makeMockDomain() BikeDomain {
	return &mockBikeDomain{}

}

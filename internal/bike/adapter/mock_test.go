package adapter

import (
	"testing"
)

func TestMockBikeMemoryStorageGet(t *testing.T) {
	storage := mockMemoryStorage{
		bikes: []Bike{
			{ID: 1, BikeBaseData: &BikeBaseData{Model: "Modelo1", Year: "Ano1", Number: 1, Status: "Status1", Brand: "Marca1"}},
		},
	}

	bikes := storage.Get()

	if len(bikes) != 1 {
		t.Errorf("expected 1 bike, got %d", len(bikes))
	}

	if bikes[0].ID != 1 {
		t.Errorf("expected bike ID 1, got %d", bikes[0].ID)
	}
}

func TestMockBikeMemoryStorageGetByID(t *testing.T) {
	storage := NewMock()

	bike := storage.GetByID(1)

	if bike.ID != mockBikeId {
		t.Errorf("expected bike ID %d, got %d", mockBikeId, bike.ID)
	}
}

func TestMockBikeMemoryStorageInsert(t *testing.T) {
	storage := NewMock()
	bikeData := BikeBaseData{Model: "Modelo2", Year: "Ano2", Number: 0, Status: "Status2", Brand: "Marca2"}

	bike := storage.Insert(bikeData)
	expectedID := storage.GetNewBikeId()
	if bike.ID != expectedID {
		t.Errorf("expected bike ID %d, got %d", expectedID, bike.ID)
	}
}

func TestMockBikeMemoryStorageUpdate(t *testing.T) {
	storage := NewMock()
	bikeData := BikeBaseData{Model: "UpdatedModel", Year: "UpdatedYear", Number: 1, Status: "UpdatedStatus", Brand: "UpdatedBrand"}

	result := storage.Update(1, bikeData)
	if !result {
		t.Errorf("expected mock to return true")
	}
}

func TestMockBikeMemoryStorageGetNewBikeId(t *testing.T) {
	storage := mockMemoryStorage{
		bikes: []Bike{
			{ID: 1},
			{ID: 2},
		},
	}

	newID := storage.GetNewBikeId()
	expectedID := 3
	if newID != expectedID {
		t.Errorf("expected new bike ID %d, got %d", expectedID, newID)
	}
}

func TestMockBikeMemoryStorageDelete(t *testing.T) {
	storage := mockMemoryStorage{
		bikes: []Bike{
			{ID: 1, BikeBaseData: &BikeBaseData{Model: "Modelo1", Year: "Ano1", Number: 1, Status: "Status1", Brand: "Marca1"}},
		},
	}

	result := storage.Delete(1)
	if !result {
		t.Errorf("expected mock to return true")
	}
}

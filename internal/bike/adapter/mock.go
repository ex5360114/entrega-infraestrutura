package adapter

func NewMock() BikeAdapter {
	mock := &mockMemoryStorage{}
	return mock
}

type mockMemoryStorage struct {
	bikes []Bike
}

// UpdateBikeStatus implements BikeAdapter.
func (b *mockMemoryStorage) UpdateBikeStatus(id int, newStatus string) bool {
	return true
}

// GetLatestBikeLockHistoryByBikeId implements BikeAdapter.
func (b *mockMemoryStorage) GetLatestBikeLockHistoryByBikeId(bikeId int) *BikeLockHistory {
	return nil
}

// InsertBikeLockHistory implements BikeAdapter.
func (b *mockMemoryStorage) InsertBikeLockHistory(bikeLockHistoryData IntegrateBikeBaseData, bikeOriginalStatus string) BikeLockHistory {
	return BikeLockHistory{}
}

var mockBikeId = 1
var mockBikeModel = "modelo"
var mockBikeYear = "ano"
var mockBikeNumber = 1
var mockBikeStatus = "status"
var mockBikeBrand = "marca"

func (b *mockMemoryStorage) Get() []Bike {
	return b.bikes
}

func (b *mockMemoryStorage) GetByID(id int) *Bike {
	return &Bike{
		ID: mockBikeId,
		BikeBaseData: &BikeBaseData{
			Model:  mockBikeModel,
			Year:   mockBikeYear,
			Number: mockBikeNumber,
			Status: mockBikeStatus,
			Brand:  mockBikeBrand,
		},
	}
}

func (b *mockMemoryStorage) Insert(bikeToCreate BikeBaseData) Bike {
	var bike = Bike{
		ID:           b.GetNewBikeId(),
		BikeBaseData: &bikeToCreate,
	}

	return bike
}

func (b *mockMemoryStorage) Update(id int, bike BikeBaseData) bool {
	return true
}

func (b *mockMemoryStorage) GetNewBikeId() int {
	return len(b.bikes) + 1
}

func (b *mockMemoryStorage) Delete(id int) bool {
	return true
}

func (b *mockMemoryStorage) SetLockId(bikeId int, lockId int) {
}

func (b *mockMemoryStorage) GetByLockId(lockId int) Bike {
	return Bike{}
}

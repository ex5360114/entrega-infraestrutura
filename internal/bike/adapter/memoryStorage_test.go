package adapter

import (
	"testing"
)

// Mock memoryStorage functions to avoid dependency issues
func TestBikeMemoryStorage_Get(t *testing.T) {
	storage := &bikeMemoryStorage{
		bikes: []*Bike{
			{ID: 1, BikeBaseData: &BikeBaseData{Model: "Modelo1", Year: "Ano1", Number: 1, Status: "Status1", Brand: "Marca1"}},
		},
	}

	bikes := storage.Get()
	if len(bikes) != 1 {
		t.Errorf("expected 1 bike, got %d", len(bikes))
	}

	if bikes[0].ID != 1 {
		t.Errorf("expected bike ID 1, got %d", bikes[0].ID)
	}
}

func TestBikeMemoryStorage_GetByID(t *testing.T) {
	storage := &bikeMemoryStorage{
		bikes: []*Bike{
			{ID: 1, BikeBaseData: &BikeBaseData{Model: "Modelo1", Year: "Ano1", Number: 1, Status: "Status1", Brand: "Marca1"}},
		},
	}

	bike := storage.GetByID(1)
	if bike.ID != 1 {
		t.Errorf("expected bike ID 1, got %d", bike.ID)
	}
}

func TestBikeMemoryStorage_Insert(t *testing.T) {
	storage := &bikeMemoryStorage{}
	bikeData := BikeBaseData{Model: "Modelo1", Year: "Ano1", Number: 1, Status: "Status1", Brand: "Marca1"}

	bike := storage.Insert(bikeData)
	if bike.ID != 1 {
		t.Errorf("expected bike ID 1, got %d", bike.ID)
	}
}

func TestBikeMemoryStorage_Update(t *testing.T) {
	storage := &bikeMemoryStorage{
		bikes: []*Bike{
			{ID: 1, BikeBaseData: &BikeBaseData{Model: "Modelo1", Year: "Ano1", Number: 1, Status: "Status1", Brand: "Marca1"}},
		},
	}
	bikeData := BikeBaseData{Model: "Modelo1", Year: "Ano1", Number: 1, Status: "Status1", Brand: "Marca1"}
	result := storage.Update(1, bikeData)
	if !result {
		t.Errorf("expected mock to return true")
	}
}

func TestBikeMemoryStorage_Delete(t *testing.T) {
	storage := &bikeMemoryStorage{
		bikes: []*Bike{
			{ID: 1, BikeBaseData: &BikeBaseData{Model: "Modelo1", Year: "Ano1", Number: 1, Status: "Status1", Brand: "Marca1"}},
		},
	}

	result := storage.Delete(1)
	if !result {
		t.Errorf("expected delete to return true")
	}

	if len(storage.bikes) != 0 {
		t.Errorf("expected 0 bikes, got %d", len(storage.bikes))
	}
}

func TestBikeMemoryStorage_GetNewBikeId(t *testing.T) {
	storage := &bikeMemoryStorage{
		bikes: []*Bike{
			{ID: 1},
			{ID: 2},
		},
	}

	newID := storage.GetNewBikeId()
	expectedID := 3
	if newID != expectedID {
		t.Errorf("expected new bike ID %d, got %d", expectedID, newID)
	}
}

func TestBikeMemoryStorage_SetLockId(t *testing.T) {
	storage := &bikeMemoryStorage{
		bikes: []*Bike{
			{ID: 1},
		},
	}

	storage.SetLockId(1, 4)

	updatedLockId := storage.bikes[0].lockId

	if updatedLockId == nil {
		t.Errorf("pointer was not set")
		return
	}

	if *updatedLockId != 4 {
		t.Errorf("expected bike lockID %d, got %d", updatedLockId, 4)
	}
}

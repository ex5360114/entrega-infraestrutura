package adapter

import (
	"entrega-infraestrutura/internal/memoryStorage"
	"time"
)

var lockID1 = 1
var lockID3 = 3
var lockID4 = 4
var sharedStorageAddress = &bikeMemoryStorage{bikes: []*Bike{
	{
		lockId: &lockID1,
		ID:     1,
		BikeBaseData: &BikeBaseData{
			Model:  "Caloi",
			Brand:  "Caloi",
			Year:   "2020",
			Number: 12345,
			Status: "DISPONIVEL",
		},
	},
	{
		lockId: &lockID3,
		ID:     2,
		BikeBaseData: &BikeBaseData{
			Model:  "Caloi",
			Brand:  "Caloi",
			Year:   "2020",
			Number: 12345,
			Status: "REPARO_SOLICITADO",
		},
	},
	{
		lockId: nil,
		ID:     3,
		BikeBaseData: &BikeBaseData{
			Model:  "Caloi",
			Brand:  "Caloi",
			Year:   "2020",
			Number: 12345,
			Status: "EM_USO",
		},
	},
	{
		lockId: nil,
		ID:     4,
		BikeBaseData: &BikeBaseData{
			Model:  "Caloi",
			Brand:  "Caloi",
			Year:   "2020",
			Number: 12345,
			Status: "EM_REPARO",
		},
	},
	{
		lockId: &lockID4,
		ID:     5,
		BikeBaseData: &BikeBaseData{
			Model:  "Caloi",
			Brand:  "Caloi",
			Year:   "2020",
			Number: 12345,
			Status: "EM_USO",
		},
	},
},
	bikeLockHistory: []BikeLockHistory{
		{
			ID:             1,
			OriginalStatus: "NOVA",
			CreatedAt:      time.Now().Format(time.DateTime),
			IntegrateBikeBaseData: IntegrateBikeBaseData{
				BikeId:     4,
				LockId:     2,
				EmployeeId: 1,
			},
		},
	}}

func NewMemoryStorage() BikeAdapter {
	return sharedStorageAddress
}

type bikeMemoryStorage struct {
	bikes           []*Bike
	bikeLockHistory []BikeLockHistory
}

func (b *bikeMemoryStorage) UpdateBikeStatus(id int, newStatus string) bool {
	bikeToBeUpdated := b.GetByID(id)
	if bikeToBeUpdated == nil {
		return false
	}
	bikeToBeUpdated.Status = newStatus
	return true
}

func (b *bikeMemoryStorage) Get() []Bike {
	bikes := []Bike{}
	for _, bikePtr := range b.bikes {
		bikes = append(bikes, *bikePtr)
	}

	return bikes
}

func (b *bikeMemoryStorage) GetByID(id int) *Bike {
	bike := memoryStorage.GetById(&b.bikes, id)
	if bike == nil {
		return nil
	}
	return *bike
}

func (b *bikeMemoryStorage) Insert(bikeToCreate BikeBaseData) Bike {
	var bike = Bike{
		ID:           b.GetNewBikeId(),
		BikeBaseData: &bikeToCreate,
	}

	memoryStorage.Insert(&b.bikes, &bike)
	insertedBike := b.GetByID(bike.ID)
	return *insertedBike
}

func (b *bikeMemoryStorage) Update(id int, bike BikeBaseData) bool {
	bikeToBeUpdated := b.GetByID(id)
	if bikeToBeUpdated == nil {
		return false
	}

	*bikeToBeUpdated.BikeBaseData = bike

	return true
}

func (b *bikeMemoryStorage) GetNewBikeId() int {
	highestId := 0
	for _, bike := range b.bikes {
		if bike.ID > highestId {
			highestId = bike.ID
		}
	}
	return highestId + 1
}

func (b *bikeMemoryStorage) SetLockId(bikeId int, lockId int) {
	bike := b.GetByID(bikeId)
	if bike == nil {
		return
	}
	bike.lockId = &lockId
}

func (b *bikeMemoryStorage) GetByLockId(lockId int) Bike {
	for _, bike := range b.bikes {
		if bike.lockId != nil && *bike.lockId == lockId {
			return *bike
		}
	}
	return Bike{}
}

func (b *bikeMemoryStorage) Delete(id int) bool {
	result := memoryStorage.Delete(&b.bikes, id)
	return result
}

func (b *bikeMemoryStorage) GetLatestBikeLockHistoryByBikeId(bikeId int) *BikeLockHistory {
	latestBikeLockPtr := &BikeLockHistory{}
	for _, bikeLockHistory := range b.bikeLockHistory {
		// Ignoring these would be bad if it was a frontend input.
		latestDate, _ := time.Parse(time.DateTime, latestBikeLockPtr.CreatedAt)
		currentItemDate, _ := time.Parse(time.DateTime, bikeLockHistory.CreatedAt)

		if bikeLockHistory.BikeId == bikeId && latestDate.Before(currentItemDate) {
			latestBikeLockPtr = &bikeLockHistory
		}
	}

	return latestBikeLockPtr
}

func (b *bikeMemoryStorage) InsertBikeLockHistory(bikeLockHistoryData IntegrateBikeBaseData, bikeOriginalStatus string) BikeLockHistory {
	newId := b.getNewBikeLockHistoryId()
	bikeLockHistory := BikeLockHistory{
		ID:                    newId,
		IntegrateBikeBaseData: bikeLockHistoryData,
		OriginalStatus:        bikeOriginalStatus,
		CreatedAt:             time.Now().Format(time.DateTime),
	}

	memoryStorage.Insert(&b.bikeLockHistory, bikeLockHistory)
	return bikeLockHistory
}

func (b *bikeMemoryStorage) getNewBikeLockHistoryId() int {
	highestId := 0
	for _, bikeLockHistory := range b.bikeLockHistory {
		if bikeLockHistory.ID > highestId {
			highestId = bikeLockHistory.ID
		}
	}
	return highestId + 1
}

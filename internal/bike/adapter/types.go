package adapter

type BikeAdapter interface {
	Get() []Bike
	GetByID(id int) *Bike
	Insert(bikeToCreate BikeBaseData) Bike
	Update(id int, Bike BikeBaseData) bool
	UpdateBikeStatus(id int, newStatus string) bool
	GetNewBikeId() int
	Delete(id int) bool
	GetByLockId(lockId int) Bike
	SetLockId(bikeId int, lockId int)

	GetLatestBikeLockHistoryByBikeId(bikeId int) *BikeLockHistory
	InsertBikeLockHistory(bikeLockHistoryData IntegrateBikeBaseData, bikeOriginalStatus string) BikeLockHistory
}

type Bike struct {
	ID     int `json:"id"`
	lockId *int
	*BikeBaseData
}

func (t Bike) GetId() int {
	return t.ID
}

type BikeBaseData struct {
	Model  string `json:"modelo"`
	Year   string `json:"ano"`
	Number int    `json:"numero"`
	Status string `json:"status"`
	Brand  string `json:"marca"`
}

type BikeLockHistory struct {
	ID int `json:"id"`
	IntegrateBikeBaseData
	OriginalStatus string `json:"statusOriginal"`
	CreatedAt      string `json:"dataCriacao"`
}

func (b BikeLockHistory) GetId() int {
	return b.ID
}

type IntegrateBikeBaseData struct {
	BikeId     int `json:"idBicicleta"`
	LockId     int `json:"idTranca"`
	EmployeeId int `json:"idFuncionario"`
}

type ReleaseBikeBaseData struct {
	BikeId     int    `json:"idBicicleta"`
	LockId     int    `json:"idTranca"`
	EmployeeId int    `json:"idFuncionario"`
	NewStatus  string `json:"statusAcaoReparador"`
}

package bike

import (
	"bytes"
	"encoding/json"
	"entrega-infraestrutura/internal/apiError"
	"entrega-infraestrutura/internal/bike/adapter"
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	"entrega-infraestrutura/internal/bike/forms"
	"entrega-infraestrutura/internal/utils"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func makeTestController() BikeController {
	return BikeController{
		Mux:                          http.NewServeMux(),
		DefaultHandlerCreator:        utils.MakeHTTPHandlerFuncMock,
		HandlerWithValidationCreator: utils.MakeHTTPHandlerWithValidationFuncMock,

		BikeDomain: makeMockDomain(),

		IdValidator: utils.MockValidateId,

		// Only physical dependencies, I need a way to write JSON to verify the controller output, otherwise the test doesnt make sense.
		BodyDecoder: utils.DecodeBody,
		JsonWriter:  utils.WriteJson,

		BikeCreationValidator:     forms.ValidateBikeCreationMock,
		BikeIntegrationValidation: forms.ValidateBikeIntegrationMock,
		BikeReleaseValidation:     forms.ValidateBikeRelease,

		InternalServerErrorCreator: apiError.InternalServerErrorApiError,
		ValidationErrorsCreator:    apiError.NewWithValidationErrors,
	}
}

func TestGetBikeHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFunc(controller.getBikeHTTPHandler))

	req, err := http.NewRequest(http.MethodGet, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(bikeAdapter.Bike{ID: 1, BikeBaseData: &bikeAdapter.BikeBaseData{
		Model:  "Modelo mockado",
		Year:   "2024",
		Number: 1,
		Status: "NOVA",
		Brand:  "marca mockada",
	}})
	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected bike object as response body, got: %v", string(body))
	}
}

func TestGetBikesHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(controller.DefaultHandlerCreator(controller.getBikesHTTPHandler))

	req, err := http.NewRequest(http.MethodGet, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(make([]bikeAdapter.Bike, 0))

	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(string(want))

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected empty bike arrays as response body, got: %v", string(body))
	}
}

func TestInsertBikeHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.insertBikeHTTPHandler))

	bodyPayload := adapter.BikeBaseData{
		Number: 1,
		Year:   "2024",
		Model:  "modelo teste",
		Status: "NOVA",
		Brand:  "marca",
	}
	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(bikeAdapter.Bike{ID: 1, BikeBaseData: &bikeAdapter.BikeBaseData{
		Number: 1,
		Year:   "2024",
		Model:  "modelo teste",
		Status: "NOVA",
		Brand:  "marca",
	}})

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected bike as response body, got: %v", string(body))
	}
}

func TestUpdateBikeHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.updateBikeHTTPHandler))

	bodyPayload := adapter.BikeBaseData{
		Number: 1,
		Year:   "2024",
		Model:  "modelo teste",
		Status: "NOVA",
		Brand:  "marca",
	}

	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(bikeAdapter.Bike{ID: 1, BikeBaseData: &bikeAdapter.BikeBaseData{
		Number: 1,
		Year:   "2024",
		Model:  "modelo teste",
		Status: "NOVA",
		Brand:  "marca",
	}})

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected bike as response body, got: %v", string(body))
	}
}

func TestDeleteBikeHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(controller.DefaultHandlerCreator(controller.deleteBikeHTTPHandler))

	req, err := http.NewRequest(http.MethodDelete, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()
}

func TestReleaseBikeHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.releaseBikeHTTPHandler))

	bodyPayload := bikeAdapter.ReleaseBikeBaseData{
		LockId:     1,
		BikeId:     1,
		EmployeeId: 1,
		NewStatus:  "EM_REPARO",
	}

	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(make(map[string]string))

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected emtpy json as response body, got: %v", string(body))
	}
}

func TestIntegrateBikeHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.integrateBikeHTTPHandler))

	bodyPayload := bikeAdapter.IntegrateBikeBaseData{
		LockId:     1,
		BikeId:     1,
		EmployeeId: 1,
	}

	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	want, err := json.Marshal(make(map[string]string))

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected empty object as response body, got: %v", string(body))
	}
}

func TestInitializeRoutes(t *testing.T) {
	controller := makeTestController()
	controller.InitializeRoutes()
}

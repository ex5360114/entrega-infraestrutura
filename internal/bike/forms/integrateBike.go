package forms

import (
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	"entrega-infraestrutura/internal/validations"
)

func ValidateBikeIntegration(data bikeAdapter.IntegrateBikeBaseData) []error {
	bikeId := validations.FieldValidation{
		FieldName:  "idBicicleta",
		FieldValue: data.BikeId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	employeeId := validations.FieldValidation{
		FieldName:  "idBicicleta",
		FieldValue: data.BikeId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	lockId := validations.FieldValidation{
		FieldName:  "idTranca",
		FieldValue: data.LockId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	validationsList := []validations.FieldValidation{
		bikeId, employeeId, lockId,
	}

	validationsErr := validations.Validator(validationsList)
	return validationsErr
}

func ValidateBikeIntegrationMock(data bikeAdapter.IntegrateBikeBaseData) []error {
	return make([]error, 0)
}

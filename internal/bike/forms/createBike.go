package forms

import (
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	"entrega-infraestrutura/internal/validations"
)

func ValidateBikeCreation(data bikeAdapter.BikeBaseData) []error {
	model := validations.FieldValidation{
		FieldName:  "modelo",
		FieldValue: data.Model,
		Validations: []validations.ValidationFunc{
			validations.IsEmptyString,
		},
	}

	brand := validations.FieldValidation{
		FieldName:  "marca",
		FieldValue: data.Brand,
		Validations: []validations.ValidationFunc{
			validations.IsEmptyString,
		},
	}

	year := validations.FieldValidation{
		FieldName:  "ano",
		FieldValue: data.Year,
		Validations: []validations.ValidationFunc{
			validations.IsEmptyString,
		},
	}

	number := validations.FieldValidation{
		FieldName:  "numero",
		FieldValue: data.Number,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	status := validations.FieldValidation{
		FieldName:  "status",
		FieldValue: data.Status,
		Validations: []validations.ValidationFunc{
			validations.IsEmptyString, validations.NewBikeStatusCreation,
		},
	}

	validationsList := []validations.FieldValidation{
		model, brand, year, number, status,
	}

	validationsErr := validations.Validator(validationsList)
	return validationsErr
}

func ValidateBikeCreationMock(data bikeAdapter.BikeBaseData) []error {
	return make([]error, 0)
}

package forms

import (
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	"errors"
	"testing"
)

func TestValidateBikeCreation(t *testing.T) {
	tests := []struct {
		data           bikeAdapter.BikeBaseData
		expectedOutput []error
	}{
		{
			bikeAdapter.BikeBaseData{
				Model:  "",
				Year:   "",
				Number: 0,
				Status: "",
				Brand:  "",
			},
			[]error{
				errors.New("modelo: string is empty"),
				errors.New("marca: string is empty"),
				errors.New("ano: string is empty"),
				errors.New("numero: number must be greater than zero"),
				errors.New("status: string is empty"),
				errors.New("status: invalid bike status"),
			},
		},
		{
			bikeAdapter.BikeBaseData{
				Model:  "modelo teste",
				Year:   "2024",
				Number: 1,
				Status: "NOVA",
				Brand:  "marca teste",
			},
			[]error{},
		},
	}

	for _, test := range tests {
		output := ValidateBikeCreation(test.data)
		if len(output) != len(test.expectedOutput) {
			t.Errorf("Expected output to have length %d, but got %d", len(test.expectedOutput), len(output))
		}

		for i, err := range output {
			if err.Error() != test.expectedOutput[i].Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput[i], err)
			}
		}
	}

}

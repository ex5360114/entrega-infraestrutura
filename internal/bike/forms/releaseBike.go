package forms

import (
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	"entrega-infraestrutura/internal/validations"
)

func ValidateBikeRelease(data bikeAdapter.ReleaseBikeBaseData) []error {
	bikeId := validations.FieldValidation{
		FieldName:  "idBicicleta",
		FieldValue: data.BikeId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	employeeId := validations.FieldValidation{
		FieldName:  "idFuncionario",
		FieldValue: data.EmployeeId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	lockId := validations.FieldValidation{
		FieldName:  "idTranca",
		FieldValue: data.LockId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	status := validations.FieldValidation{
		FieldName:  "statusAcaoReparador",
		FieldValue: data.NewStatus,
		Validations: []validations.ValidationFunc{
			validations.LockReleaseStatus,
		},
	}

	validationsList := []validations.FieldValidation{
		bikeId, employeeId, lockId, status,
	}

	validationsErr := validations.Validator(validationsList)
	return validationsErr
}

func ValidateLockReleaseMock(data bikeAdapter.ReleaseBikeBaseData) []error {
	return make([]error, 0)
}

package bike

import (
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	"testing"
)

func TestBikeDomainInsert(t *testing.T) {
	domain := &bikeDomain{adapter: bikeAdapter.NewMock()}

	bikeData := bikeAdapter.BikeBaseData{
		Model:  "modelo",
		Year:   "ano",
		Number: 1,
		Status: "status",
		Brand:  "marca",
	}

	bike, apiErr := domain.Insert(bikeData)
	if apiErr != nil {
		t.Errorf("expected no error, got %v", apiErr)
	}

	if bike.ID != 1 {
		t.Errorf("Expected buke ID 1, got %d", bike.ID)
	}

	if bike.Model != bikeData.Model || bike.Year != bikeData.Year || bike.Number != bikeData.Number || bike.Status != bikeData.Status || bike.Brand != bikeData.Brand {
		t.Errorf("insertion should not alter a field")
	}
}

func TestBikeDomainGet(t *testing.T) {
	domain := &bikeDomain{adapter: bikeAdapter.NewMock()}

	bikes, apiErr := domain.Get()
	if apiErr != nil {
		t.Errorf("domain should not have returned error")
	}

	if len(bikes) != 0 {
		t.Errorf("storage was suposed to be empty, domain.Get() should not return anything")
	}
}

func TestBikeDomainUpdate(t *testing.T) {
	domain := &bikeDomain{adapter: bikeAdapter.NewMock()}
	bikeData := bikeAdapter.BikeBaseData{
		Model:  "modelo",
		Year:   "ano",
		Number: 1,
		Status: "status",
		Brand:  "marca",
	}

	bike, apiErr := domain.Update(1, bikeData)
	if apiErr != nil {
		t.Errorf("domain should not have returned error")
	}

	if bikeData.Status != bike.Status {
		t.Errorf("Expected bike with specific status, got other status")
	}
}

func TestBikeDomainDelete(t *testing.T) {
	domain := &bikeDomain{adapter: bikeAdapter.NewMock()}
	apiErr := domain.Delete(1)

	if apiErr != nil {
		t.Errorf("delete domain usecase should not return an error on mocked adapter returns")
	}
}

func TestBikeDomainGetById(t *testing.T) {
	domain := &bikeDomain{adapter: bikeAdapter.NewMock()}

	_, err := domain.GetByID(1)

	if err != nil {
		t.Errorf("getById domain usecase should not return an error on mocked adapter returns")

	}
}

package bike

import (
	"entrega-infraestrutura/internal/apiError"
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	"entrega-infraestrutura/internal/external"
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"entrega-infraestrutura/internal/rental"
	"errors"
	"fmt"
	"net/http"
)

type BikeDomain interface {
	Get() ([]bikeAdapter.Bike, *apiError.ApiError)
	GetByID(id int) (*bikeAdapter.Bike, *apiError.ApiError)
	Insert(bikeData bikeAdapter.BikeBaseData) (bikeAdapter.Bike, *apiError.ApiError)
	Update(id int, bike bikeAdapter.BikeBaseData) (bikeAdapter.Bike, *apiError.ApiError)
	Delete(id int) *apiError.ApiError
	IncludeBikeOnLock(data bikeAdapter.IntegrateBikeBaseData) *apiError.ApiError
	ReleaseBikeFromLock(data bikeAdapter.ReleaseBikeBaseData) *apiError.ApiError
}

type bikeDomain struct {
	adapter     bikeAdapter.BikeAdapter
	lockAdapter lockAdapter.Adapter

	rentalService   rental.RentalService
	externalService external.ExternalService
}

func NewInMemory() BikeDomain {
	bikeAdapter := bikeAdapter.NewMemoryStorage()
	lockAdapter := lockAdapter.NewMemoryStorage()
	rentalService := rental.MakeMockRentalService()
	externalService := external.MakeMockExternalService()

	return &bikeDomain{
		adapter:         bikeAdapter,
		lockAdapter:     lockAdapter,
		rentalService:   rentalService,
		externalService: externalService,
	}
}

func (b *bikeDomain) Insert(bikeData bikeAdapter.BikeBaseData) (bikeAdapter.Bike, *apiError.ApiError) {
	bike := b.adapter.Insert(bikeData)
	return bike, nil
}

func (b *bikeDomain) Get() ([]bikeAdapter.Bike, *apiError.ApiError) {
	bikes := b.adapter.Get()
	return bikes, nil
}

func (b *bikeDomain) GetByID(id int) (*bikeAdapter.Bike, *apiError.ApiError) {
	bike := b.adapter.GetByID(id)
	if bike == nil {
		return nil, apiError.AssetNotFoundApiError()
	}

	return bike, nil
}

func (b *bikeDomain) Update(id int, bikeData bikeAdapter.BikeBaseData) (bikeAdapter.Bike, *apiError.ApiError) {
	bikeBeforeUpdate, apiErr := b.GetByID(id)
	if apiErr != nil {
		return bikeAdapter.Bike{}, apiErr
	}

	if bikeBeforeUpdate.Number != bikeData.Number {
		return bikeAdapter.Bike{}, apiError.New(errors.New("o numero da bicicleta nao pode ser editado"), 400)
	}

	result := b.adapter.Update(id, bikeData)
	if !result {
		return bikeAdapter.Bike{}, apiError.InternalServerErrorApiError(errors.New("não foi possivel atualizar a bike"))
	}

	bike := b.adapter.GetByID(id)
	if apiErr != nil {
		return bikeAdapter.Bike{}, apiErr
	}

	return *bike, nil
}

func (b *bikeDomain) Delete(id int) *apiError.ApiError {
	_, apiErr := b.GetByID(id)
	if apiErr != nil {
		return apiErr
	}

	result := b.adapter.Delete(id)

	if !result {
		return apiError.InternalServerErrorApiError(errors.New("não foi possivel deletar a bike"))

	}

	return nil
}

func (b *bikeDomain) IncludeBikeOnLock(data bikeAdapter.IntegrateBikeBaseData) *apiError.ApiError {
	bike := b.adapter.GetByID(data.BikeId)
	if bike == nil {
		return apiError.AssetNotFoundApiError()
	}

	if bike.Status == "EM_REPARO" {
		latestBikeTotemHistory := b.adapter.GetLatestBikeLockHistoryByBikeId(bike.ID)
		if latestBikeTotemHistory == nil {
			return apiError.InternalServerErrorApiError(errors.New("uma bicicleta em reparo deveria ter um registro associado ao reparo"))
		}

		if latestBikeTotemHistory.EmployeeId != data.EmployeeId {
			return apiError.New(errors.New("apenas o funcionario que realizou o reparo pode integrar a bicicleta novamente"), http.StatusBadRequest)
		}
	}

	lock := b.lockAdapter.GetByID(data.LockId)
	if lock == nil {
		return apiError.AssetNotFoundApiError()
	}

	employee, err := b.rentalService.GetEmployeeById(data.EmployeeId)
	if err != nil {
		return apiError.InternalServerErrorApiError(errors.New("nao foi possivel encontrar informacoes sobre funcionario"))
	}

	historyRow := b.adapter.InsertBikeLockHistory(data, bike.Status)
	b.adapter.UpdateBikeStatus(bike.ID, "DISPONIVEL")
	b.adapter.SetLockId(bike.ID, data.LockId)

	err = b.externalService.SendEmail(external.SendEmailParams{
		Email:   employee.Email,
		Subject: "Inclusao de bicicleta",
		Message: fmt.Sprintf("Voce incluiu a bicicleta de ID %v na tranca de ID %v no dia %v", bike.ID, lock.ID, historyRow.CreatedAt),
	})

	if err != nil {
		return apiError.InternalServerErrorApiError(errors.New("nao foi possivel enviar email para funcionario"))
	}

	return nil
}

func (b *bikeDomain) ReleaseBikeFromLock(data bikeAdapter.ReleaseBikeBaseData) *apiError.ApiError {
	bike := b.adapter.GetByID(data.BikeId)
	if bike == nil {
		return apiError.AssetNotFoundApiError()
	}

	lock := b.lockAdapter.GetByID(data.LockId)
	if lock == nil {
		return apiError.AssetNotFoundApiError()
	}

	employee, err := b.rentalService.GetEmployeeById(data.EmployeeId)
	if err != nil {
		return apiError.InternalServerErrorApiError(errors.New("nao foi possivel encontrar informacoes sobre funcionario"))
	}

	historyRow := b.adapter.InsertBikeLockHistory(bikeAdapter.IntegrateBikeBaseData{
		LockId:     data.LockId,
		BikeId:     data.BikeId,
		EmployeeId: data.EmployeeId,
	}, bike.Status)

	b.adapter.UpdateBikeStatus(bike.ID, data.NewStatus)
	b.adapter.SetLockId(bike.ID, 0)

	err = b.externalService.SendEmail(external.SendEmailParams{
		Email:   employee.Email,
		Subject: "Retirada de bicicleta",
		Message: fmt.Sprintf("Voce retirou a bicicleta de ID %v da tranca de ID %v no dia %v", bike.ID, lock.ID, historyRow.CreatedAt),
	})

	if err != nil {
		return apiError.InternalServerErrorApiError(errors.New("nao foi possivel enviar email para funcionario"))
	}

	return nil
}

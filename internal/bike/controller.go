package bike

import (
	"entrega-infraestrutura/internal/apiError"
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	"entrega-infraestrutura/internal/utils"
	"errors"
	"net/http"
)

type BikeController struct {
	//* General Dependencies for any controller (can be refactored)
	// HTTP server mux
	Mux *http.ServeMux

	// HTTP handler creators
	DefaultHandlerCreator        func(f utils.ApiFunc) http.HandlerFunc
	HandlerWithValidationCreator func(f utils.ValidatedApiFunc) http.HandlerFunc

	// utils required
	IdValidator func(string) (int, *apiError.ApiError)
	JsonWriter  func(http.ResponseWriter, int, any) error
	BodyDecoder func(r http.Request, bodyStorage interface{}) error

	// Error constructor functions
	InternalServerErrorCreator func(err error) *apiError.ApiError
	ValidationErrorsCreator    func(messages []error) []apiError.ApiError

	//* Domain-specific dependencies
	// Domains used
	BikeDomain BikeDomain

	// validation forms
	BikeCreationValidator     func(data bikeAdapter.BikeBaseData) []error
	BikeIntegrationValidation func(data bikeAdapter.IntegrateBikeBaseData) []error
	BikeReleaseValidation     func(data bikeAdapter.ReleaseBikeBaseData) []error
}

// Assigning Handlers to Routes
func (c *BikeController) InitializeRoutes() {
	c.Mux.HandleFunc("GET /bicicleta/{id}", c.DefaultHandlerCreator(c.getBikeHTTPHandler))
	c.Mux.HandleFunc("PUT /bicicleta/{id}", c.HandlerWithValidationCreator(c.updateBikeHTTPHandler))
	c.Mux.HandleFunc("DELETE /bicicleta/{id}", c.DefaultHandlerCreator(c.deleteBikeHTTPHandler))
	c.Mux.HandleFunc("POST /bicicleta", c.HandlerWithValidationCreator(c.insertBikeHTTPHandler))
	c.Mux.HandleFunc("GET /bicicleta", c.DefaultHandlerCreator(c.getBikesHTTPHandler))
	c.Mux.HandleFunc("POST /bicicleta/integrarNaRede", c.HandlerWithValidationCreator(c.integrateBikeHTTPHandler))
	c.Mux.HandleFunc("POST /bicicleta/retirarDaRede", c.HandlerWithValidationCreator(c.releaseBikeHTTPHandler))

}

// Declaring Handlers
func (c *BikeController) getBikeHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, err := c.IdValidator(idReceived)

	if err != nil {
		return err
	}

	bike, err := c.BikeDomain.GetByID(idParsed)
	if err != nil {
		return err
	}

	c.JsonWriter(w, 200, bike)

	return nil
}

func (c *BikeController) getBikesHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	bikes, err := c.BikeDomain.Get()
	if err != nil {
		return err
	}

	c.JsonWriter(w, 200, bikes)

	return nil
}

func (c *BikeController) insertBikeHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	var bikeData bikeAdapter.BikeBaseData
	err := c.BodyDecoder(*r, &bikeData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := c.BikeCreationValidator(bikeData)
	if len(validationErrs) > 0 {
		return c.ValidationErrorsCreator(validationErrs)
	}

	bike, apiErr := c.BikeDomain.Insert(bikeData)
	if apiErr != nil {
		return []apiError.ApiError{
			*apiErr,
		}
	}

	c.JsonWriter(w, 200, bike)

	return nil
}

func (c *BikeController) updateBikeHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, idValidationErr := c.IdValidator(idReceived)
	if idValidationErr != nil {
		idErr := errors.New(idValidationErr.Message)
		return c.ValidationErrorsCreator([]error{idErr})
	}

	var bikeData bikeAdapter.BikeBaseData
	err := c.BodyDecoder(*r, &bikeData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := c.BikeCreationValidator(bikeData)
	if len(validationErrs) > 0 {
		return apiError.NewWithValidationErrors(validationErrs)
	}

	bike, updateUsecaseError := c.BikeDomain.Update(idParsed, bikeData)
	if updateUsecaseError != nil {
		return []apiError.ApiError{*updateUsecaseError}
	}

	c.JsonWriter(w, 200, bike)

	return nil
}

func (c *BikeController) deleteBikeHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, apiErr := c.IdValidator(idReceived)

	if apiErr != nil {
		return apiErr
	}

	apiErr = c.BikeDomain.Delete(idParsed)
	if apiErr != nil {
		return apiErr
	}

	c.JsonWriter(w, 200, apiErr)

	return nil
}

func (c *BikeController) integrateBikeHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	var bikeData bikeAdapter.IntegrateBikeBaseData
	err := c.BodyDecoder(*r, &bikeData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := c.BikeIntegrationValidation(bikeData)
	if len(validationErrs) > 0 {
		return c.ValidationErrorsCreator(validationErrs)
	}

	updateUsecaseError := c.BikeDomain.IncludeBikeOnLock(bikeData)
	if updateUsecaseError != nil {
		return []apiError.ApiError{*updateUsecaseError}
	}

	c.JsonWriter(w, 200, make(map[string]string))

	return nil
}

func (c *BikeController) releaseBikeHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	var bikeData bikeAdapter.ReleaseBikeBaseData
	err := c.BodyDecoder(*r, &bikeData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := c.BikeReleaseValidation(bikeData)
	if len(validationErrs) > 0 {
		return c.ValidationErrorsCreator(validationErrs)
	}

	updateUsecaseError := c.BikeDomain.ReleaseBikeFromLock(bikeData)
	if updateUsecaseError != nil {
		return []apiError.ApiError{*updateUsecaseError}
	}

	c.JsonWriter(w, 200, make(map[string]string))

	return nil
}

package rental

import (
	"encoding/json"
	"errors"
	"net/http"
)

func MakeMockRentalService() RentalService {
	return &mockRentalService{}
}

type mockRentalService struct{}

func (m *mockRentalService) GetEmployeeById(id int) (Employee, error) {
	response, err := http.Get("http://134.209.68.180:4040/enviarEmail")
	if err == nil {
		return Employee{}, err
	}
	var employee *Employee
	decoder := json.NewDecoder(response.Body)
	err = decoder.Decode(employee)
	if err == nil {
		return Employee{}, err
	}
	if employee == nil {
		return Employee{}, errors.New("could not decode GetEmployeeByID response")
	}
	return *employee, err
}

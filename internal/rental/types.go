package rental

type RentalService interface {
	GetEmployeeById(id int) (Employee, error)
	// Uncomment if needed
	// GetBikerById(id int) (Biker, error)
}

type Employee struct {
	Id                   int    `json:"id"`
	RegistrationCode     string `json:"matricula"`
	Password             string `json:"senha"`
	PasswordConfirmation string `json:"confirmacaoSenha"` // This field is probably a mistake on the Swagger definition...
	Email                string `json:"email"`
	Name                 string `json:"name"`
	Age                  int    `json:"idade"`
	Area                 string `json:"funcao"`
	Cpf                  string `json:"cpf"`
}

type Cyclist struct {
	Id               string   `json:"id"`
	Status           string   `json:"status"`
	Name             string   `json:"nome"`
	BirthDate        string   `json:"nascimento"`
	Cpf              string   `json:"cpf"`
	Passport         Passport `json:"passaporte"`
	Nationality      string   `json:"nacionalidade"`
	Email            string   `json:"email"`
	DocumentPhotoUrl string   `json:"urlFotoDocumento"`
}

type Passport struct {
	Number         string `json:"numero"`
	ExpirationDate string `json:"validade"`
	Country        string `json:"pais"`
}

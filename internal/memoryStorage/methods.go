package memoryStorage

/*
This is a general implementation for basic CRUD in-memory operations.
All items stored in-memory with this package MUST implement the RetrievableById interface.
This basically means that all items must contain a method that returns an integer used to identify that item.
*/

type RetrievableById interface {
	GetId() (id int)
}

func Insert[T RetrievableById](list *[]T, item T) {
	*list = append(*list, item)
}

/*
Since GetById already returns a pointer to a struct, it is not necessary to implement an Update at this level.
If someone using this package needs to perform an Update, all it needs is to  update the values returned by this function.
*/
func GetById[T RetrievableById](list *[]T, id int) *T {
	for _, item := range *list {
		itemId := item.GetId()

		if int(itemId) == id {
			return &item
		}
	}
	return nil
}

func Delete[T RetrievableById](list *[]T, id int) bool {
	for i, item := range *list {
		itemId := item.GetId()

		if itemId == id {
			// This creates a new list containing all the items from indexes 0 to "i" AND "i+1" until the end
			*list = append((*list)[:i], (*list)[i+1:]...)
			return true
		}
	}
	return false
}

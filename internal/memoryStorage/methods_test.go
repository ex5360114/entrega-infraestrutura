package memoryStorage

import (
	"testing"
)

type Person struct {
	ID   int
	Name string
}

func (p Person) GetId() int {
	return p.ID
}

func TestInsertSinglePerson(t *testing.T) {
	var PersonList = []Person{
		{
			ID:   1,
			Name: "John Doe",
		},
	}

	personToInsert := Person{
		ID: 2,
	}

	Insert(&PersonList, personToInsert)

	if len(PersonList) != 2 {
		t.Errorf("Expected list length to be 2, but got %d", len(PersonList))
	}
}

func TestGetPersonById(t *testing.T) {
	var PersonList = []Person{
		{
			ID:   1,
			Name: "John Doe",
		},
		{
			ID:   2,
			Name: "Jane Doe",
		},
		{
			ID:   3,
			Name: "John Smith",
		},
	}

	lock := GetById(&PersonList, 1)

	if lock == nil {
		t.Errorf("Expected lock to be not nil, but got %v", lock)
	}

	if lock.ID != 1 {
		t.Errorf("Expected lock ID to be 1, but got %d", lock.ID)
	}
}

func TestDeleteAPerson(t *testing.T) {
	var PersonList = []Person{
		{
			ID:   1,
			Name: "John Doe",
		},
		{
			ID:   2,
			Name: "Jane Doe",
		},
		{
			ID:   3,
			Name: "John Smith",
		},
	}

	var result = Delete(&PersonList, 2)

	if len(PersonList) != 2 {
		t.Errorf("Expected list length to be 2, but got %d", len(PersonList))
	}

	if result == false {
		t.Errorf("Expected delete call to return true. Returned false.")
	}
}

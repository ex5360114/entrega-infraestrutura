package totem

import (
	"entrega-infraestrutura/internal/apiError"
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	totemAdapter "entrega-infraestrutura/internal/totem/adapter"
	"errors"
)

type TotemDomain interface {
	Insert(totemData totemAdapter.TotemBaseData) (totemAdapter.Totem, *apiError.ApiError)
	Get() ([]totemAdapter.Totem, *apiError.ApiError)
	GetByID(id int) (*totemAdapter.Totem, *apiError.ApiError)
	Update(id int, totem totemAdapter.TotemBaseData) (totemAdapter.Totem, *apiError.ApiError)
	Delete(id int) *apiError.ApiError
	GetAllLocksFromTotem(totemId int) []lockAdapter.Lock
	GetAllBikesFromTotem(totemId int) []bikeAdapter.Bike
}

type totemDomain struct {
	adapter     totemAdapter.TotemAdapter
	lockAdapter lockAdapter.Adapter
	bikeAdapter bikeAdapter.BikeAdapter
}

func NewInMemory() TotemDomain {
	totemAdapter := totemAdapter.NewMemoryStorage()
	lockAdapter := lockAdapter.NewMemoryStorage()
	bikeAdapter := bikeAdapter.NewMemoryStorage()

	return &totemDomain{
		adapter:     totemAdapter,
		lockAdapter: lockAdapter,
		bikeAdapter: bikeAdapter,
	}
}

func (t *totemDomain) Insert(totemData totemAdapter.TotemBaseData) (totemAdapter.Totem, *apiError.ApiError) {
	totem := t.adapter.Insert(totemData)
	return totem, nil
}

func (t *totemDomain) Get() ([]totemAdapter.Totem, *apiError.ApiError) {
	totems := t.adapter.Get()
	return totems, nil
}

func (t *totemDomain) GetByID(id int) (*totemAdapter.Totem, *apiError.ApiError) {
	totem := t.adapter.GetByID(id)
	if totem == nil {
		return nil, apiError.AssetNotFoundApiError()
	}

	return totem, nil
}

func (t *totemDomain) Update(id int, totemData totemAdapter.TotemBaseData) (totemAdapter.Totem, *apiError.ApiError) {
	_, apiErr := t.GetByID(id)
	if apiErr != nil {
		return totemAdapter.Totem{}, apiErr
	}

	result := t.adapter.Update(id, totemData)
	if !result {
		return totemAdapter.Totem{}, apiError.InternalServerErrorApiError(errors.New("não foi possivel atualizar o totem"))
	}

	totem := t.adapter.GetByID(id)

	return *totem, nil
}

func (t *totemDomain) Delete(id int) *apiError.ApiError {
	_, apiErr := t.GetByID(id)
	if apiErr != nil {
		return apiErr
	}

	result := t.adapter.Delete(id)
	if !result {
		return apiError.InternalServerErrorApiError(errors.New("não foi possivel deletar o totem"))
	}

	return nil
}

func (t *totemDomain) GetAllLocksFromTotem(totemId int) []lockAdapter.Lock {
	locks := t.lockAdapter.GetAllByTotemId(totemId)
	return locks
}

func (t *totemDomain) GetAllBikesFromTotem(totemId int) []bikeAdapter.Bike {
	bikes := []bikeAdapter.Bike{}

	locks := t.lockAdapter.GetAllByTotemId(totemId)

	for _, lock := range locks {
		bike := t.bikeAdapter.GetByLockId(lock.ID)
		if bike.ID != 0 {
			bikes = append(bikes, bike)
		}
	}

	return bikes
}

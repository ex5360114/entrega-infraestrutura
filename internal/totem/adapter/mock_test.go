package adapter

import (
	"testing"
)

func TestMockTotemMemoryStorage_Get(t *testing.T) {
	storage := &mockTotemMemoryStorage{
		totems: []Totem{
			{ID: 1, TotemBaseData: &TotemBaseData{Location: "Location1", Description: "Description1"}},
		},
	}

	totems := storage.Get()

	if len(totems) != 1 {
		t.Errorf("expected 1 totem, got %d", len(totems))
	}

	if totems[0].ID != 1 {
		t.Errorf("expected totem ID 1, got %d", totems[0].ID)
	}
}

func TestMockTotemMemoryStorage_GetByID(t *testing.T) {
	storage := NewMock()

	totem := storage.GetByID(1)

	if totem.ID != mockTotemId {
		t.Errorf("expected totem ID %d, got %d", mockTotemId, totem.ID)
	}

	if totem.Location != mockTotemLocation {
		t.Errorf("expected totem location %s, got %s", mockTotemLocation, totem.Location)
	}

	if totem.Description != mockTotemDescription {
		t.Errorf("expected totem description %s, got %s", mockTotemDescription, totem.Description)
	}
}

func TestMockTotemMemoryStorage_Insert(t *testing.T) {
	storage := NewMock()
	totemData := TotemBaseData{Location: "Location2", Description: "Description2"}

	totem := storage.Insert(totemData)
	expectedID := storage.GetNewTotemId()
	if totem.ID != expectedID {
		t.Errorf("expected totem ID %d, got %d", expectedID, totem.ID)
	}

	if totem.Location != totemData.Location {
		t.Errorf("expected totem location %s, got %s", totemData.Location, totem.Location)
	}

	if totem.Description != totemData.Description {
		t.Errorf("expected totem description %s, got %s", totemData.Description, totem.Description)
	}
}

func TestMockTotemMemoryStorage_Update(t *testing.T) {
	storage := NewMock()
	totemData := TotemBaseData{Location: "UpdatedLocation", Description: "UpdatedDescription"}

	result := storage.Update(1, totemData)
	if !result {
		t.Errorf("expected mock to return true")
	}
}

func TestMockTotemMemoryStorage_GetNewTotemId(t *testing.T) {
	storage := &mockTotemMemoryStorage{
		totems: []Totem{
			{ID: 1},
			{ID: 2},
		},
	}

	newID := storage.GetNewTotemId()
	expectedID := 3
	if newID != expectedID {
		t.Errorf("expected new totem ID %d, got %d", expectedID, newID)
	}
}

func TestMockTotemMemoryStorage_Delete(t *testing.T) {
	storage := &mockTotemMemoryStorage{
		totems: []Totem{
			{ID: 1, TotemBaseData: &TotemBaseData{Location: "Location1", Description: "Description1"}},
		},
	}

	result := storage.Delete(1)
	if !result {
		t.Errorf("expected mock to return true")
	}
}

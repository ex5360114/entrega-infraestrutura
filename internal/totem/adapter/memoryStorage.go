package adapter

import "entrega-infraestrutura/internal/memoryStorage"

var sharedAdapterAddress = &totemMemoryStorage{totems: []Totem{
	{
		ID: 1,
		TotemBaseData: &TotemBaseData{
			Location:    "Rio de Janeiro",
			Description: "Tranca exemplo",
		},
	},
}}

func NewMemoryStorage() TotemAdapter {
	return sharedAdapterAddress
}

type totemMemoryStorage struct {
	totems []Totem
}

func (b *totemMemoryStorage) Get() []Totem {
	return b.totems
}

func (b *totemMemoryStorage) GetByID(id int) *Totem {
	totem := memoryStorage.GetById(&b.totems, id)
	return totem
}

func (b *totemMemoryStorage) Insert(totemToCreate TotemBaseData) Totem {
	var totem = Totem{
		ID:            b.GetNewTotemId(),
		TotemBaseData: &totemToCreate,
	}

	memoryStorage.Insert(&b.totems, totem)
	insertedTotem := b.GetByID(totem.ID)
	return *insertedTotem
}

func (b *totemMemoryStorage) Update(id int, totem TotemBaseData) bool {
	totemToBeUpdated := b.GetByID(id)
	if totemToBeUpdated.ID == 0 {
		return false
	}

	*totemToBeUpdated.TotemBaseData = totem

	return true
}

func (b *totemMemoryStorage) GetNewTotemId() int {
	highestId := 0
	for _, totem := range b.totems {
		if totem.ID > highestId {
			highestId = totem.ID
		}
	}
	return highestId + 1
}

func (b *totemMemoryStorage) Delete(id int) bool {
	result := memoryStorage.Delete(&b.totems, id)
	return result
}

package adapter

func NewMock() TotemAdapter {
	memoryStorage := &mockTotemMemoryStorage{}
	return memoryStorage
}

type mockTotemMemoryStorage struct {
	totems []Totem
}

var mockTotemId = 1
var mockTotemLocation = "location"
var mockTotemDescription = "description"

func (b *mockTotemMemoryStorage) Get() []Totem {
	return b.totems
}

func (b *mockTotemMemoryStorage) GetByID(id int) *Totem {
	return &Totem{
		ID: mockTotemId,
		TotemBaseData: &TotemBaseData{
			Location:    mockTotemLocation,
			Description: mockTotemDescription,
		},
	}
}

func (b *mockTotemMemoryStorage) Insert(totemToCreate TotemBaseData) Totem {
	var totem = Totem{
		ID:            b.GetNewTotemId(),
		TotemBaseData: &totemToCreate,
	}

	return totem
}

func (b *mockTotemMemoryStorage) Update(id int, totem TotemBaseData) bool {
	return true
}

func (b *mockTotemMemoryStorage) GetNewTotemId() int {
	return len(b.totems) + 1
}

func (b *mockTotemMemoryStorage) Delete(id int) bool {
	return true
}

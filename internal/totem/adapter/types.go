package adapter

type TotemAdapter interface {
	Get() []Totem
	GetByID(id int) *Totem
	Insert(totemToCreate TotemBaseData) Totem
	Update(id int, Totem TotemBaseData) bool
	GetNewTotemId() int
	Delete(id int) bool
}

type Totem struct {
	ID int `json:"id"`
	*TotemBaseData
}

func (t Totem) GetId() int {
	return t.ID
}

type TotemBaseData struct {
	Location    string `json:"localizacao"`
	Description string `json:"descricao"`
}

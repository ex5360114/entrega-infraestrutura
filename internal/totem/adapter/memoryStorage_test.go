package adapter

import (
	"testing"
)

// Mock memoryStorage functions to avoid dependency issues
func TestTotemMemoryStorage_Get(t *testing.T) {
	storage := &totemMemoryStorage{
		totems: []Totem{
			{ID: 1, TotemBaseData: &TotemBaseData{Location: "Location1", Description: "Description1"}},
		},
	}

	totems := storage.Get()
	if len(totems) != 1 {
		t.Errorf("expected 1 totem, got %d", len(totems))
	}

	if totems[0].ID != 1 {
		t.Errorf("expected totem ID 1, got %d", totems[0].ID)
	}
}

func TestTotemMemoryStorage_GetByID(t *testing.T) {
	storage := &totemMemoryStorage{
		totems: []Totem{
			{ID: 1, TotemBaseData: &TotemBaseData{Location: "Location1", Description: "Description1"}},
		},
	}

	totem := storage.GetByID(1)
	if totem.ID != 1 {
		t.Errorf("expected totem ID 1, got %d", totem.ID)
	}
}

func TestTotemMemoryStorage_Insert(t *testing.T) {
	storage := &totemMemoryStorage{}
	totemData := TotemBaseData{Location: "Location1", Description: "Description1"}

	totem := storage.Insert(totemData)
	if totem.ID != 1 {
		t.Errorf("expected totem ID 1, got %d", totem.ID)
	}
}

func TestTotemMemoryStorage_Update(t *testing.T) {
	storage := &totemMemoryStorage{
		totems: []Totem{
			{ID: 1, TotemBaseData: &TotemBaseData{Location: "Location1", Description: "Description1"}},
		},
	}
	totemData := TotemBaseData{Location: "Location1 updated", Description: "Description1 updated"}
	result := storage.Update(1, totemData)
	if !result {
		t.Errorf("expected method to return true")
	}

	if storage.totems[0].Description != totemData.Description {
		t.Errorf("adapter update operation did not update the data")
	}
}

func TestTotemMemoryStorage_Delete(t *testing.T) {
	storage := &totemMemoryStorage{
		totems: []Totem{
			{ID: 1, TotemBaseData: &TotemBaseData{Location: "Location1", Description: "Description1"}},
		},
	}

	result := storage.Delete(1)
	if !result {
		t.Errorf("expected delete to return true")
	}

	if len(storage.totems) != 0 {
		t.Errorf("expected 0 totems, got %d", len(storage.totems))
	}
}

func TestTotemMemoryStorage_GetNewTotemId(t *testing.T) {
	storage := &totemMemoryStorage{
		totems: []Totem{
			{ID: 1},
			{ID: 2},
		},
	}

	newID := storage.GetNewTotemId()
	expectedID := 3
	if newID != expectedID {
		t.Errorf("expected new totem ID %d, got %d", expectedID, newID)
	}
}

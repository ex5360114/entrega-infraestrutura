package totem

import (
	"entrega-infraestrutura/internal/apiError"
	totemAdapter "entrega-infraestrutura/internal/totem/adapter"
	"entrega-infraestrutura/internal/totem/forms"
	"entrega-infraestrutura/internal/utils"
	"net/http"
)

type TotemController struct {
	//* General Dependencies for any controller (can be refactored)
	// HTTP server mux
	Mux *http.ServeMux

	// HTTP handler creators
	DefaultHandlerCreator        func(f utils.ApiFunc) http.HandlerFunc
	HandlerWithValidationCreator func(f utils.ValidatedApiFunc) http.HandlerFunc

	// utils required
	IdValidator func(string) (int, *apiError.ApiError)
	JsonWriter  func(http.ResponseWriter, int, any) error
	BodyDecoder func(r http.Request, bodyStorage interface{}) error

	// Error constructor functions
	InternalServerErrorCreator func(err error) *apiError.ApiError
	ValidationErrorsCreator    func(messages []error) []apiError.ApiError

	//* Domain-specific dependencies
	// Domains used
	TotemDomain TotemDomain

	// validation forms
	TotemCreationValidator func(data totemAdapter.TotemBaseData) []error
}

// Assigning Handlers to Routes
func (c *TotemController) InitializeRoutes() {
	c.Mux.HandleFunc("GET /totem/{id}", c.DefaultHandlerCreator(c.getTotemHTTPHandler))
	c.Mux.HandleFunc("GET /totem/{id}/trancas", c.DefaultHandlerCreator(c.getLocksFromTotemHTTPHandler))
	c.Mux.HandleFunc("GET /totem/{id}/bicicletas", c.DefaultHandlerCreator(c.getBikesFromTotemHTTPHandler))
	c.Mux.HandleFunc("PUT /totem/{id}", c.HandlerWithValidationCreator(c.updateTotemHTTPHandler))
	c.Mux.HandleFunc("DELETE /totem/{id}", c.DefaultHandlerCreator(c.deleteTotemHTTPHandler))
	c.Mux.HandleFunc("POST /totem", c.HandlerWithValidationCreator(c.insertTotemHTTPHandler))
	c.Mux.HandleFunc("GET /totem", c.DefaultHandlerCreator(c.getTotemsHTTPHandler))
}

// Declaring Handlers
func (c *TotemController) getTotemHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, err := c.IdValidator(idReceived)

	if err != nil {
		return err
	}

	totem, err := c.TotemDomain.GetByID(idParsed)
	if err != nil {
		return err
	}

	c.JsonWriter(w, 200, totem)

	return nil
}

func (c *TotemController) getTotemsHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	totems, err := c.TotemDomain.Get()
	if err != nil {
		return err
	}

	c.JsonWriter(w, 200, totems)

	return nil
}

func (c *TotemController) insertTotemHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	var totemData totemAdapter.TotemBaseData

	err := c.BodyDecoder(*r, &totemData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := forms.ValidateTotemCreation(totemData)
	if len(validationErrs) > 0 {
		return c.ValidationErrorsCreator(validationErrs)
	}

	totem, apiErr := c.TotemDomain.Insert(totemData)
	if apiErr != nil {
		return []apiError.ApiError{
			*apiErr,
		}
	}

	c.JsonWriter(w, 200, totem)

	return nil
}

func (c *TotemController) updateTotemHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, idValidationErr := c.IdValidator(idReceived)
	if idValidationErr != nil {
		return []apiError.ApiError{*idValidationErr}
	}

	var totemData totemAdapter.TotemBaseData
	err := c.BodyDecoder(*r, &totemData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := forms.ValidateTotemCreation(totemData)
	if len(validationErrs) > 0 {
		return apiError.NewWithValidationErrors(validationErrs)
	}

	totem, updateUsecaseError := c.TotemDomain.Update(idParsed, totemData)
	if updateUsecaseError != nil {
		return []apiError.ApiError{*updateUsecaseError}
	}

	c.JsonWriter(w, 200, totem)

	return nil
}

func (c *TotemController) deleteTotemHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, apiErr := c.IdValidator(idReceived)

	if apiErr != nil {
		return apiErr
	}

	apiErr = c.TotemDomain.Delete(idParsed)
	if apiErr != nil {
		return apiErr
	}

	c.JsonWriter(w, 200, apiErr)

	return nil
}

func (c *TotemController) getLocksFromTotemHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, apiErr := c.IdValidator(idReceived)

	if apiErr != nil {
		return apiErr
	}

	locks := c.TotemDomain.GetAllLocksFromTotem(idParsed)

	c.JsonWriter(w, 200, locks)

	return nil
}

func (c *TotemController) getBikesFromTotemHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, apiErr := c.IdValidator(idReceived)

	if apiErr != nil {
		return apiErr
	}

	bikes := c.TotemDomain.GetAllBikesFromTotem(idParsed)

	c.JsonWriter(w, 200, bikes)

	return nil
}

package totem

import (
	"bytes"
	"encoding/json"
	totemAdapter "entrega-infraestrutura/internal/totem/adapter"
	"entrega-infraestrutura/internal/totem/forms"
	"entrega-infraestrutura/internal/utils"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func makeTestController() TotemController {
	return TotemController{
		Mux:                          http.NewServeMux(),
		DefaultHandlerCreator:        utils.MakeHTTPHandlerFuncMock,
		HandlerWithValidationCreator: utils.MakeHTTPHandlerWithValidationFuncMock,

		TotemDomain: makeMockDomain(),

		IdValidator: utils.MockValidateId,

		// Only physical dependencies, I need a way to write JSON to verify the controller output, otherwise the test doesnt make sense.
		BodyDecoder: utils.DecodeBody,
		JsonWriter:  utils.WriteJson,

		TotemCreationValidator: forms.ValidateTotemCreationMock,
	}
}

func TestGetTotemHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFunc(controller.getTotemHTTPHandler))

	req, err := http.NewRequest(http.MethodGet, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(totemAdapter.Totem{ID: 1, TotemBaseData: &totemAdapter.TotemBaseData{
		Location:    "Location mock",
		Description: "Description mock",
	}})
	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected totem object as response body, got: %v", string(body))
	}
}

func TestGetTotemsHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(controller.DefaultHandlerCreator(controller.getTotemsHTTPHandler))

	req, err := http.NewRequest(http.MethodGet, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(make([]totemAdapter.Totem, 0))

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected empty totem array as response body, got: %v", string(body))
	}
}

func TestInsertTotemHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.insertTotemHTTPHandler))

	bodyPayload := totemAdapter.TotemBaseData{
		Location:    "Location mock",
		Description: "Description mock",
	}
	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(totemAdapter.Totem{ID: 1, TotemBaseData: &bodyPayload})
	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected totem as response body, got: %v", string(body))
	}
}

func TestUpdateTotemHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.insertTotemHTTPHandler))

	bodyPayload := totemAdapter.TotemBaseData{
		Location:    "Location mock",
		Description: "Description mock",
	}
	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(totemAdapter.Totem{ID: 1, TotemBaseData: &bodyPayload})
	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected totem as response body, got: %v", string(body))
	}
}

func TestDeleteTotemHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(controller.DefaultHandlerCreator(controller.deleteTotemHTTPHandler))

	req, err := http.NewRequest(http.MethodDelete, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()
}

func TestInitializeRoutes(t *testing.T) {
	controller := makeTestController()
	controller.InitializeRoutes()
}

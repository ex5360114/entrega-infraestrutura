package totem

import (
	totemAdapter "entrega-infraestrutura/internal/totem/adapter"
	"testing"
)

func TestTotemDomainInsert(t *testing.T) {
	domain := &totemDomain{adapter: totemAdapter.NewMock()}

	totemData := totemAdapter.TotemBaseData{
		Location:    "location",
		Description: "description",
	}

	totem, apiErr := domain.Insert(totemData)

	if apiErr != nil {
		t.Errorf("expected no error, got %v", apiErr)
	}

	if totem.Description != totemData.Description || totem.Location != totemData.Location {
		t.Errorf("insertion should not alter a field")
	}
}

func TestTotemDomainGet(t *testing.T) {
	domain := &totemDomain{adapter: totemAdapter.NewMock()}
	totems, apiErr := domain.Get()
	if apiErr != nil {
		t.Errorf("domain should not have returned error")
	}

	if len(totems) != 0 {
		t.Errorf("storage was suposed to be empty, domain.Get() should not return anything")
	}
}

func TestTotemDomainUpdate(t *testing.T) {
	domain := &totemDomain{adapter: totemAdapter.NewMock()}
	totemData := totemAdapter.TotemBaseData{
		Location:    "location",
		Description: "description",
	}

	_, apiErr := domain.Update(1, totemData)
	if apiErr != nil {
		t.Errorf("domain should not have returned error")
	}
}

func TestTotemDomainDelete(t *testing.T) {
	domain := &totemDomain{adapter: totemAdapter.NewMock()}
	apiErr := domain.Delete(1)

	if apiErr != nil {
		t.Errorf("delete domain usecase should not return an error on mocked adapter returns")
	}
}

func TestTotemDomainGetById(t *testing.T) {
	domain := &totemDomain{adapter: totemAdapter.NewMock()}
	_, err := domain.GetByID(1)
	if err != nil {
		t.Errorf("getById domain usecase should not return an error on mocked adapter returns")
	}
}

// Integration Test

func TestIntegrationTotemDomainUpdate(t *testing.T) {
	domain := &totemDomain{adapter: totemAdapter.NewMemoryStorage()}
	insertedTotem, err := domain.Insert(
		totemAdapter.TotemBaseData{
			Location:    "location",
			Description: "description",
		})
	if err != nil {
		t.Errorf("insertion should not return error")
	}

	if insertedTotem.ID == 0 {
		t.Errorf("insertion should not return totem with ID 0")
	}

	totens, err := domain.Get()
	if err != nil {
		t.Errorf("Get totens should not return error")
	}
	if len(totens) == 0 {
		t.Errorf("Get totens should have returned at least one totem")
	}

	totemData := totemAdapter.TotemBaseData{
		Location:    "location updated",
		Description: "description updated",
	}

	updatedTotem, apiErr := domain.Update(1, totemData)
	if apiErr != nil {
		t.Errorf("domain should not have returned error")
	}

	if updatedTotem.Description != totemData.Description {
		t.Errorf("update usecase did not update the totem data")
	}
}

package totem

import (
	"entrega-infraestrutura/internal/apiError"
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	totemAdapter "entrega-infraestrutura/internal/totem/adapter"
)

type mockDomain struct{}

func (t *mockDomain) Insert(totemData totemAdapter.TotemBaseData) (totemAdapter.Totem, *apiError.ApiError) {
	return totemAdapter.Totem{ID: 1, TotemBaseData: &totemData}, nil
}

func (t *mockDomain) Get() ([]totemAdapter.Totem, *apiError.ApiError) {
	return make([]totemAdapter.Totem, 0), nil
}

func (t *mockDomain) GetByID(id int) (*totemAdapter.Totem, *apiError.ApiError) {
	return &totemAdapter.Totem{ID: 1, TotemBaseData: &totemAdapter.TotemBaseData{
		Location:    "Location mock",
		Description: "Description mock",
	}}, nil
}

func (t *mockDomain) Update(id int, totemData totemAdapter.TotemBaseData) (totemAdapter.Totem, *apiError.ApiError) {
	return totemAdapter.Totem{ID: 1, TotemBaseData: &totemData}, nil
}

func (t *mockDomain) Delete(id int) *apiError.ApiError {
	return nil
}

func (t *mockDomain) GetAllLocksFromTotem(totemId int) []lockAdapter.Lock {
	return nil
}

func (t *mockDomain) GetAllBikesFromTotem(totemId int) []bikeAdapter.Bike {
	return nil
}

func makeMockDomain() TotemDomain {
	return &mockDomain{}
}

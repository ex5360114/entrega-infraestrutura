package forms

import (
	totemAdapter "entrega-infraestrutura/internal/totem/adapter"
	"errors"
	"testing"
)

func TestValidateTotemCreation(t *testing.T) {
	tests := []struct {
		data           totemAdapter.TotemBaseData
		expectedOutput []error
	}{
		{
			data: totemAdapter.TotemBaseData{Location: "", Description: ""},
			expectedOutput: []error{
				errors.New("localizacao: string is empty"),
				errors.New("descricao: string is empty"),
			},
		},
	}

	for _, test := range tests {
		output := ValidateTotemCreation(test.data)
		if len(output) != len(test.expectedOutput) {
			t.Errorf("Expected output to have length %d, but got %d", len(test.expectedOutput), len(output))
		}

		for i, err := range output {
			if err.Error() != test.expectedOutput[i].Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput[i], err)
			}
		}
	}

}

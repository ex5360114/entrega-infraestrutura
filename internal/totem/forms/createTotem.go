package forms

import (
	totemAdapter "entrega-infraestrutura/internal/totem/adapter"
	"entrega-infraestrutura/internal/validations"
)

func ValidateTotemCreation(data totemAdapter.TotemBaseData) []error {
	location := validations.FieldValidation{
		FieldName:  "localizacao",
		FieldValue: data.Location,
		Validations: []validations.ValidationFunc{
			validations.IsEmptyString,
		},
	}

	description := validations.FieldValidation{
		FieldName:  "descricao",
		FieldValue: data.Description,
		Validations: []validations.ValidationFunc{
			validations.IsEmptyString,
		},
	}

	validationsList := []validations.FieldValidation{
		location, description,
	}
	validationsErr := validations.Validator(validationsList)
	return validationsErr
}

func ValidateTotemCreationMock(data totemAdapter.TotemBaseData) []error {
	return make([]error, 0)
}

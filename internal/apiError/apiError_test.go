package apiError

import (
	"errors"
	"net/http"
	"strconv"
	"testing"
)

func TestNew(t *testing.T) {
	message := errors.New("Test error")
	status := strconv.Itoa(http.StatusBadRequest)

	apiErr := New(message, http.StatusBadRequest)

	if apiErr.Message != message.Error() {
		t.Errorf("Expected message %s, got %s", message.Error(), apiErr.Message)
	}

	if apiErr.Status != status {
		t.Errorf("Expected status %v, got %v", status, apiErr.Status)
	}
}

func TestRouteNotFoundApiError(t *testing.T) {
	apiErr := RouteNotFoundApiError()

	expectedMessage := "route not found"
	expectedStatus := strconv.Itoa(http.StatusNotFound)

	if apiErr.Message != expectedMessage {
		t.Errorf("Expected message %s, got %s", expectedMessage, apiErr.Message)
	}

	if apiErr.Status != expectedStatus {
		t.Errorf("Expected status %s, got %s", expectedStatus, apiErr.Status)
	}
}

func TestInvalidIdApiError(t *testing.T) {
	apiErr := InvalidIdApiError()

	expectedMessage := "invalid ID received"
	expectedStatus := strconv.Itoa(http.StatusUnprocessableEntity)

	if apiErr.Message != expectedMessage {
		t.Errorf("Expected message %s, got %s", expectedMessage, apiErr.Message)
	}

	if apiErr.Status != expectedStatus {
		t.Errorf("Expected status %s, got %s", expectedStatus, apiErr.Status)
	}
}

func TestInternalServerErrorApiError(t *testing.T) {
	err := errors.New("internal server error")
	apiErr := InternalServerErrorApiError(err)

	expectedMessage := "internal server error"
	expectedStatus := strconv.Itoa(http.StatusInternalServerError)

	if apiErr.Message != expectedMessage {
		t.Errorf("Expected message %s, got %s", expectedMessage, apiErr.Message)
	}

	if apiErr.Status != expectedStatus {
		t.Errorf("Expected status %s, got %s", expectedStatus, apiErr.Status)
	}
}

func TestAssetNotFoundApiError(t *testing.T) {
	apiErr := AssetNotFoundApiError()

	expectedMessage := "asset not found"
	expectedStatus := strconv.Itoa(http.StatusNotFound)

	if apiErr.Message != expectedMessage {
		t.Errorf("Expected message %s, got %s", expectedMessage, apiErr.Message)
	}

	if apiErr.Status != expectedStatus {
		t.Errorf("Expected status %s, got %s", expectedStatus, apiErr.Status)
	}
}

func TestRouteNotFoundError(t *testing.T) {
	err := RouteNotFoundError()

	expectedMessage := "route not found"
	if err.Error() != expectedMessage {
		t.Errorf("Expected message %s, got %s", expectedMessage, err.Error())
	}
}

func TestInvalidIdReceivedError(t *testing.T) {
	err := InvalidIdReceivedError()

	expectedMessage := "invalid ID received"
	if err.Error() != expectedMessage {
		t.Errorf("Expected message %s, got %s", expectedMessage, err.Error())
	}
}

func TestAssetNotFoundError(t *testing.T) {
	err := AssetNotFoundError()

	expectedMessage := "asset not found"
	if err.Error() != expectedMessage {
		t.Errorf("Expected message %s, got %s", expectedMessage, err.Error())
	}
}

func TestNewWithValidationErrors(t *testing.T) {
	messages := []error{
		errors.New("Error 1"),
		errors.New("Error 2"),
	}

	apiErrors := NewWithValidationErrors(messages)

	if len(apiErrors) != len(messages) {
		t.Errorf("Expected %d errors, got %d", len(messages), len(apiErrors))
	}

	for i, apiErr := range apiErrors {
		if apiErr.Message != messages[i].Error() {
			t.Errorf("Expected message %s, got %s", messages[i].Error(), apiErr.Message)
		}

		if apiErr.Status != strconv.Itoa(http.StatusUnprocessableEntity) {
			t.Errorf("Expected status %d, got %s", http.StatusUnprocessableEntity, apiErr.Status)
		}
	}
}

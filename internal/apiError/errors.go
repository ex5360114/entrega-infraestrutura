package apiError

import (
	"fmt"
	"net/http"
	"strconv"
)

type ApiError struct {
	Message string `json:"mensagem"`
	Status  string `json:"codigo"`
}

func New(message error, status int) *ApiError {
	return &ApiError{
		Message: message.Error(),
		Status:  strconv.Itoa(status),
	}
}

func NewWithValidationErrors(messages []error) []ApiError {
	errors := []ApiError{}

	for _, message := range messages {
		errors = append(errors, *New(message, http.StatusUnprocessableEntity))
	}

	return errors
}

func RouteNotFoundApiError() *ApiError {
	return New(RouteNotFoundError(), http.StatusNotFound)
}

func RouteNotFoundError() error {
	return fmt.Errorf("route not found")
}

func InvalidIdApiError() *ApiError {
	return New(InvalidIdReceivedError(), http.StatusUnprocessableEntity)
}

func InvalidIdReceivedError() error {
	return fmt.Errorf("invalid ID received")
}

func InternalServerErrorApiError(err error) *ApiError {
	return New(err, http.StatusInternalServerError)
}

func InternalServerErrorApiMock(err error) *ApiError {
	return nil
}

func AssetNotFoundApiError() *ApiError {
	return New(AssetNotFoundError(), http.StatusNotFound)
}

func AssetNotFoundError() error {
	return fmt.Errorf("asset not found")
}

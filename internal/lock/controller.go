package lock

import (
	"entrega-infraestrutura/internal/apiError"
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"entrega-infraestrutura/internal/utils"
	"net/http"
)

type LockController struct {
	// HTTP server mux
	Mux *http.ServeMux

	// HTTP handler creators
	DefaultHandlerCreator        func(f utils.ApiFunc) http.HandlerFunc
	HandlerWithValidationCreator func(f utils.ValidatedApiFunc) http.HandlerFunc

	// utils required
	IdValidator func(string) (int, *apiError.ApiError)
	JsonWriter  func(http.ResponseWriter, int, any) error
	BodyDecoder func(r http.Request, bodyStorage interface{}) error

	// Error constructor functions
	InternalServerErrorCreator func(err error) *apiError.ApiError
	ValidationErrorsCreator    func(messages []error) []apiError.ApiError

	//* Domain-specific dependencies
	// Domains used
	LockDomain LockDomain

	// validation forms
	LockCreationValidation     func(data lockAdapter.LockBaseData) []error
	LockIntegrationValidation  func(data lockAdapter.IntegrateLockBaseData) []error
	LockReleaseValidation      func(data lockAdapter.ReleaseLockBaseData) []error
	LockStatusUpdateValidation func(status string) (string, []error)
	BikeAssociationValidation  func(data lockAdapter.AssociateBikeBody) []error
}

// Assigning Handlers to Routes
func (c *LockController) InitializeRoutes() {
	c.Mux.HandleFunc("GET /tranca/{id}", c.DefaultHandlerCreator(c.getLockHTTPHandler))
	c.Mux.HandleFunc("GET /tranca/{id}/bicicleta", c.HandlerWithValidationCreator(c.getBikeByLockId))
	c.Mux.HandleFunc("PUT /tranca/{id}", c.HandlerWithValidationCreator(c.updateLockHTTPHandler))
	c.Mux.HandleFunc("DELETE /tranca/{id}", c.DefaultHandlerCreator(c.deleteLockHTTPHandler))
	c.Mux.HandleFunc("POST /tranca/{id}/trancar", c.HandlerWithValidationCreator(c.lockLockHTTPHandlerStatus))
	c.Mux.HandleFunc("POST /tranca/{id}/destrancar", c.HandlerWithValidationCreator(c.unlockLockHTTPHandlerStatus))
	c.Mux.HandleFunc("POST /tranca/{id}/status/{acao}", c.HandlerWithValidationCreator(c.updateLockHTTPHandlerStatus))
	c.Mux.HandleFunc("POST /tranca", c.HandlerWithValidationCreator(c.insertLockHTTPHandler))
	c.Mux.HandleFunc("GET /tranca", c.DefaultHandlerCreator(c.getLocksHTTPHandler))
	c.Mux.HandleFunc("POST /tranca/integrarNaRede", c.HandlerWithValidationCreator(c.integrateLockHTTPHandler))
	c.Mux.HandleFunc("POST /tranca/retirarDaRede", c.HandlerWithValidationCreator(c.releaseLockHTTPHandler))
}

// Declaring Handlers
func (c *LockController) getLockHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, err := c.IdValidator(idReceived)

	if err != nil {
		return err
	}

	lock, err := c.LockDomain.GetByID(idParsed)
	if err != nil {
		return err
	}

	c.JsonWriter(w, 200, lock)

	return nil
}

func (c *LockController) getLocksHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	locks, err := c.LockDomain.Get()
	if err != nil {
		return err
	}

	c.JsonWriter(w, 200, locks)

	return nil
}

func (c *LockController) insertLockHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	var lockData lockAdapter.LockBaseData
	err := c.BodyDecoder(*r, &lockData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationsErr := c.LockCreationValidation(lockData)
	if len(validationsErr) > 0 {
		return c.ValidationErrorsCreator(validationsErr)
	}

	lock, apiErr := c.LockDomain.Insert(lockData)
	if apiErr != nil {
		return []apiError.ApiError{
			*apiErr,
		}
	}

	c.JsonWriter(w, 200, lock)

	return nil
}

func (c *LockController) updateLockHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, idValidationErr := c.IdValidator(idReceived)
	if idValidationErr != nil {
		return []apiError.ApiError{*idValidationErr}
	}

	var lockData lockAdapter.LockBaseData
	err := c.BodyDecoder(*r, &lockData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := c.LockCreationValidation(lockData)
	if len(validationErrs) > 0 {
		return c.ValidationErrorsCreator(validationErrs)
	}

	updatedLock, updateUsecaseError := c.LockDomain.Update(idParsed, lockData)
	if updateUsecaseError != nil {
		return []apiError.ApiError{*updateUsecaseError}
	}

	c.JsonWriter(w, 200, updatedLock)

	return nil
}

func (c *LockController) updateLockHTTPHandlerStatus(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	// Parsing values from path
	idReceived := r.PathValue("id")
	statusReceived := r.PathValue("acao")

	// Applying Validations
	validationErrors := []apiError.ApiError{}
	idParsed, idValidationErr := c.IdValidator(idReceived)
	if idValidationErr != nil {
		validationErrors = append(validationErrors, *idValidationErr)
	}

	statusParsed, statusValidationErr := c.LockStatusUpdateValidation(statusReceived)
	statusValidationErrorsFormatted := c.ValidationErrorsCreator(statusValidationErr)
	for i := 0; i < len(statusValidationErrorsFormatted); i++ {
		validationErrors = append(validationErrors, statusValidationErrorsFormatted[i])
	}

	if len(validationErrors) > 0 {
		return validationErrors
	}

	// Executing usecase logic
	updatedLock, updateErr := c.LockDomain.UpdateLockStatus(idParsed, 0, statusParsed)
	if updateErr != nil {
		return []apiError.ApiError{*updateErr}
	}

	c.JsonWriter(w, 200, updatedLock)

	return nil
}

func (c *LockController) lockLockHTTPHandlerStatus(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, idValidationErr := c.IdValidator(idReceived)
	if idValidationErr != nil {
		return []apiError.ApiError{*idValidationErr}
	}

	var associationData lockAdapter.AssociateBikeBody
	err := c.BodyDecoder(*r, &associationData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := c.BikeAssociationValidation(associationData)
	if len(validationErrs) > 0 {
		return c.ValidationErrorsCreator(validationErrs)
	}

	updatedLock, updateErr := c.LockDomain.UpdateLockStatus(idParsed, associationData.Bike, "trancar")
	if updateErr != nil {
		return []apiError.ApiError{*updateErr}
	}

	c.JsonWriter(w, 200, updatedLock)

	return nil
}

func (c *LockController) unlockLockHTTPHandlerStatus(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, idValidationErr := c.IdValidator(idReceived)
	if idValidationErr != nil {
		return []apiError.ApiError{*idValidationErr}
	}

	var associationData lockAdapter.AssociateBikeBody
	err := c.BodyDecoder(*r, &associationData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := c.BikeAssociationValidation(associationData)
	if len(validationErrs) > 0 {
		return c.ValidationErrorsCreator(validationErrs)
	}

	updatedLock, updateErr := c.LockDomain.UpdateLockStatus(idParsed, associationData.Bike, "destrancar")
	if updateErr != nil {
		return []apiError.ApiError{*updateErr}
	}

	c.JsonWriter(w, 200, updatedLock)

	return nil
}

func (c *LockController) deleteLockHTTPHandler(w http.ResponseWriter, r *http.Request) *apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, apiErr := c.IdValidator(idReceived)

	if apiErr != nil {
		return apiErr
	}

	apiErr = c.LockDomain.Delete(idParsed)
	if apiErr != nil {
		return apiErr
	}

	c.JsonWriter(w, 200, apiErr)

	return nil
}

func (c *LockController) integrateLockHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	var lockData lockAdapter.IntegrateLockBaseData
	err := c.BodyDecoder(*r, &lockData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := c.LockIntegrationValidation(lockData)
	if len(validationErrs) > 0 {
		return c.ValidationErrorsCreator(validationErrs)
	}

	updateUsecaseError := c.LockDomain.IncludeLockOnTotem(lockData)
	if updateUsecaseError != nil {
		return []apiError.ApiError{*updateUsecaseError}
	}

	c.JsonWriter(w, 200, make(map[string]string))

	return nil
}

func (c *LockController) releaseLockHTTPHandler(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	var lockData lockAdapter.ReleaseLockBaseData
	err := c.BodyDecoder(*r, &lockData)
	if err != nil {
		return c.ValidationErrorsCreator([]error{err})
	}

	validationErrs := c.LockReleaseValidation(lockData)
	if len(validationErrs) > 0 {
		return c.ValidationErrorsCreator(validationErrs)
	}

	updateUsecaseError := c.LockDomain.ReleaseLockFromTotem(lockData)
	if updateUsecaseError != nil {
		return []apiError.ApiError{*updateUsecaseError}
	}

	c.JsonWriter(w, 200, make(map[string]string))

	return nil
}

func (c *LockController) getBikeByLockId(w http.ResponseWriter, r *http.Request) []apiError.ApiError {
	idReceived := r.PathValue("id")
	idParsed, idValidationErr := c.IdValidator(idReceived)
	if idValidationErr != nil {
		return []apiError.ApiError{*idValidationErr}
	}

	bike, getByIdError := c.LockDomain.GetBikeByLockId(idParsed)
	if getByIdError != nil {
		return []apiError.ApiError{*getByIdError}
	}

	c.JsonWriter(w, 200, bike)

	return nil
}

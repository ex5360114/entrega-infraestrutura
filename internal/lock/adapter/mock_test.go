package adapter

import (
	"testing"
)

func TestMockLockAdapter_Get(t *testing.T) {
	mockStorage := &mockAdapter{
		locks: []Lock{
			{
				ID: 1,
				LockBaseData: &LockBaseData{
					Number:          1,
					Location:        "location1",
					ManufactureYear: "2020",
					Model:           "model1",
					Status:          "NOVA",
				},
			},
		},
	}

	locks := mockStorage.Get()

	if len(locks) != 1 {
		t.Errorf("expected 1 lock, got %d", len(locks))
	}

	if locks[0].ID != 1 {
		t.Errorf("expected lock ID 1, got %d", locks[0].ID)
	}
}

func TestMockLockAdapter_GetByID(t *testing.T) {
	mockStorage := &mockAdapter{}

	lock := mockStorage.GetByID(1)
	if lock.ID != mockLockId {
		t.Errorf("expected lock ID %d, got %d", mockLockId, lock.ID)
	}
}

func TestMockLockAdapter_Insert(t *testing.T) {
	mockStorage := &mockAdapter{}
	lockData := LockBaseData{
		Number:          2,
		Location:        "new location",
		ManufactureYear: "2021",
		Model:           "new model",
		Status:          "Available",
	}

	lock := mockStorage.Insert(lockData)

	if lock.ID != 2 {
		t.Errorf("expected lock ID 2, got %d", lock.ID)
	}
}

func TestMockLockAdapter_Update(t *testing.T) {
	mockStorage := &mockAdapter{}
	lockData := LockBaseData{
		Number:          2,
		Location:        "updated location",
		ManufactureYear: "2021",
		Model:           "updated model",
		Status:          "EM_USO",
	}

	result := mockStorage.Update(1, lockData)

	if result != true {
		t.Errorf("expected mock to return true")
	}
}

func TestMockLockAdapter_UpdateLockStatus(t *testing.T) {
	mockStorage := &mockAdapter{}

	result := mockStorage.UpdateLockStatus(1, "DISPONIVEL")

	if result != true {
		t.Errorf("expected mock to return true")
	}
}

func TestMockLockAdapter_Delete(t *testing.T) {
	mockStorage := &mockAdapter{}

	result := mockStorage.Delete(1)
	if result != true {
		t.Errorf("expected mock to return true")
	}
}

func TestMockLockAdapter_GetLatestLockTotemHistoryByLockId(t *testing.T) {
	mockStorage := &mockAdapter{}

	history := mockStorage.GetLatestLockTotemHistoryByLockId(1)
	if history.LockId != 1 {
		t.Errorf("expected lock ID 1, got %d", history.LockId)
	}
}

func TestMockLockAdapter_InsertLockTotemHistory(t *testing.T) {
	mockStorage := &mockAdapter{}
	historyData := IntegrateLockBaseData{
		TotemId:    2,
		LockId:     1,
		EmployeeId: 2,
	}

	history := mockStorage.InsertLockTotemHistory(historyData, "NOVA")

	if history.LockId != 1 {
		t.Errorf("expected lock ID 1, got %d", history.LockId)
	}

	if history.TotemId != 2 {
		t.Errorf("expected totem ID 2, got %d", history.TotemId)
	}

	if history.EmployeeId != 2 {
		t.Errorf("expected employee ID 2, got %d", history.EmployeeId)
	}
}

func TestMockLockAdapter_GetNewLockId(t *testing.T) {
	mockStorage := &mockAdapter{
		locks: []Lock{
			{
				ID: 1,
				LockBaseData: &LockBaseData{
					Number:          1,
					Location:        "location1",
					ManufactureYear: "2020",
					Model:           "model1",
					Status:          "NOVA",
				},
			},
			{
				ID: 2,
				LockBaseData: &LockBaseData{
					Number:          2,
					Location:        "location2",
					ManufactureYear: "2021",
					Model:           "model2",
					Status:          "NOVA",
				},
			},
		},
	}

	newID := mockStorage.GetNewLockId()
	expectedID := 3
	if newID != expectedID {
		t.Errorf("expected new lock ID %d, got %d", expectedID, newID)
	}
}

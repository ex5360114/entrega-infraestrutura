package adapter

import (
	"testing"
	"time"
)

func TestLockMemoryStorage_Get(t *testing.T) {
	storage := &memoryStorageAdapter{
		locks: []*Lock{
			{
				ID: 1,
				LockBaseData: &LockBaseData{
					Number:          1,
					Location:        "location1",
					ManufactureYear: "2020",
					Model:           "model1",
					Status:          "Available",
				},
			},
		},
	}

	locks := storage.Get()

	if len(locks) != 1 {
		t.Errorf("expected 1 lock, got %d", len(locks))
	}

	if locks[0].ID != 1 {
		t.Errorf("expected lock ID 1, got %d", locks[0].ID)
	}
}

func TestLockMemoryStorage_GetByID(t *testing.T) {
	storage := &memoryStorageAdapter{
		locks: []*Lock{
			{
				ID: 1,
				LockBaseData: &LockBaseData{
					Number:          1,
					Location:        "location1",
					ManufactureYear: "2020",
					Model:           "model1",
					Status:          "Available",
				},
			},
		},
	}

	lock := storage.GetByID(1)

	if lock.ID != 1 {
		t.Errorf("expected lock ID 1, got %d", lock.ID)
	}
}

func TestLockMemoryStorage_Insert(t *testing.T) {
	storage := &memoryStorageAdapter{}
	lockData := LockBaseData{
		Number:          2,
		Location:        "new location",
		ManufactureYear: "2021",
		Model:           "new model",
		Status:          "Available",
	}

	lock := storage.Insert(lockData)

	if lock.ID != 1 {
		t.Errorf("expected lock ID 1, got %d", lock.ID)
	}
}

func TestLockMemoryStorage_Update(t *testing.T) {
	storage := &memoryStorageAdapter{
		locks: []*Lock{
			{
				ID: 1,
				LockBaseData: &LockBaseData{
					Number:          1,
					Location:        "location1",
					ManufactureYear: "2020",
					Model:           "model1",
					Status:          "Available",
				},
			},
		},
	}
	lockData := LockBaseData{
		Number:          1,
		Location:        "updated location",
		ManufactureYear: "2021",
		Model:           "updated model",
		Status:          "In Use",
	}

	result := storage.Update(1, lockData)

	if !result {
		t.Errorf("expected mock to return true")
	}
}

func TestLockMemoryStorage_UpdateLockStatus(t *testing.T) {
	storage := &memoryStorageAdapter{
		locks: []*Lock{
			{
				ID: 1,
				LockBaseData: &LockBaseData{
					Number:          1,
					Location:        "location1",
					ManufactureYear: "2020",
					Model:           "model1",
					Status:          "Available",
				},
			},
		},
	}

	result := storage.UpdateLockStatus(1, "DISPONIVEL")
	if result != true {
		t.Errorf("expected test to return true")
	}

	updatedLockPtr := storage.GetByID(1)
	if updatedLockPtr == nil || updatedLockPtr.LockBaseData == nil {
		t.Errorf("expected lock to be found")
	}

	baseDataFields := updatedLockPtr.LockBaseData
	if baseDataFields.Status != "DISPONIVEL" {
		t.Errorf("expected lock status to be updated")
	}
}

func TestLockMemoryStorage_Delete(t *testing.T) {
	storage := &memoryStorageAdapter{
		locks: []*Lock{
			{
				ID: 1,
				LockBaseData: &LockBaseData{
					Number:          1,
					Location:        "location1",
					ManufactureYear: "2020",
					Model:           "model1",
					Status:          "Available",
				},
			},
		},
	}

	result := storage.Delete(1)
	if result != true {
		t.Errorf("Expected deletion to return true.")
	}

	if len(storage.locks) != 0 {
		t.Errorf("expected 0 locks, got %d", len(storage.locks))
	}
}

func TestLockMemoryStorage_GetLatestLockTotemHistoryByLockId(t *testing.T) {
	storage := &memoryStorageAdapter{
		lockTotemHistory: []LockTotemHistory{
			{
				ID: 1,
				IntegrateLockBaseData: IntegrateLockBaseData{
					TotemId:    1,
					LockId:     1,
					EmployeeId: 1,
				},
				OriginalStatus: "status1",
				CreatedAt:      time.Now().Format(time.DateTime),
			},
		},
	}

	history := storage.GetLatestLockTotemHistoryByLockId(1)

	if history.LockId != 1 {
		t.Errorf("expected lock ID 1, got %d", history.LockId)
	}
}

func TestLockMemoryStorage_InsertLockTotemHistory(t *testing.T) {
	storage := &memoryStorageAdapter{}
	historyData := IntegrateLockBaseData{
		TotemId:    1,
		LockId:     1,
		EmployeeId: 1,
	}

	history := storage.InsertLockTotemHistory(historyData, "status1")

	if history.LockId != 1 {
		t.Errorf("expected lock ID 1, got %d", history.LockId)
	}

	if history.TotemId != 1 {
		t.Errorf("expected totem ID 1, got %d", history.TotemId)
	}

	if history.EmployeeId != 1 {
		t.Errorf("expected employee ID 1, got %d", history.EmployeeId)
	}
}

func TestLockMemoryStorage_GetNewLockId(t *testing.T) {
	storage := &memoryStorageAdapter{
		locks: []*Lock{
			{ID: 1},
			{ID: 2},
			{ID: 4},
		},
	}

	newID := storage.GetNewLockId()
	expectedID := 5
	if newID != expectedID {
		t.Errorf("expected new lock ID %d, got %d", expectedID, newID)
	}
}

func TestLockMemoryStorage_getNewLockTotemHistoryId(t *testing.T) {
	storage := &memoryStorageAdapter{
		lockTotemHistory: []LockTotemHistory{
			{ID: 1},
			{ID: 2},
			{ID: 4},
		},
	}

	newID := storage.getNewLockTotemHistoryId()
	expectedID := 5
	if newID != expectedID {
		t.Errorf("expected new lock totem history ID %d, got %d", expectedID, newID)
	}
}
func TestBikeMemoryStorage_SetTotemId(t *testing.T) {
	storage := &memoryStorageAdapter{
		locks: []*Lock{
			{ID: 1},
		},
	}

	storage.SetTotemId(1, 4)

	updatedTotemId := storage.locks[0].totemId

	if updatedTotemId == nil {
		t.Errorf("pointer was not set")
		return
	}

	if *updatedTotemId != 4 {
		t.Errorf("expected lock totemID %d, got %d", updatedTotemId, 4)
	}
}

package adapter

import "time"

func NewMock() Adapter {
	mock := &mockAdapter{locks: make([]Lock, 0), lockTotemHistory: make([]LockTotemHistory, 0)}
	return mock
}

type mockAdapter struct {
	locks            []Lock
	lockTotemHistory []LockTotemHistory
}

var mockLockId = 1
var mockLockNumber = 1
var mockLockLocation = "location"
var mockLockManufactureYear = "2020"
var mockLockModel = "model"
var mockLockStatus = "Available"

func (b *mockAdapter) Get() []Lock {
	return b.locks
}

func (b *mockAdapter) GetByID(id int) *Lock {
	return &Lock{
		ID: mockLockId,
		LockBaseData: &LockBaseData{
			Number:          mockLockNumber,
			Location:        mockLockLocation,
			ManufactureYear: mockLockManufactureYear,
			Model:           mockLockModel,
			Status:          mockLockStatus,
		},
	}
}

func (b *mockAdapter) Insert(lockToCreate LockBaseData) Lock {
	return Lock{
		ID:           2,
		LockBaseData: &lockToCreate,
	}
}

func (b *mockAdapter) Update(id int, lockToCreate LockBaseData) bool {
	return true
}

func (b *mockAdapter) UpdateLockStatus(id int, newStatus string) bool {
	return true
}

func (b *mockAdapter) GetNewLockId() int {
	return len(b.locks) + 1
}

func (b *mockAdapter) Delete(id int) bool {
	return true
}

func (b *mockAdapter) GetLatestLockTotemHistoryByLockId(lockId int) *LockTotemHistory {
	return &LockTotemHistory{
		ID: 1,
		IntegrateLockBaseData: IntegrateLockBaseData{
			TotemId:    1,
			LockId:     lockId,
			EmployeeId: 1,
		},
		OriginalStatus: "",
		CreatedAt:      time.Now().String(),
	}
}

func (b *mockAdapter) InsertLockTotemHistory(lockTotemHistoryData IntegrateLockBaseData, lockOriginalStatus string) LockTotemHistory {
	return LockTotemHistory{
		ID:                    1,
		IntegrateLockBaseData: lockTotemHistoryData,
		OriginalStatus:        lockOriginalStatus,
		CreatedAt:             time.Now().String(),
	}
}
func (b *mockAdapter) SetTotemId(lockId int, totemId int) {
}

func (b *mockAdapter) GetAllByTotemId(totemId int) []Lock {
	return []Lock{}
}

package adapter

type Adapter interface {
	Get() []Lock
	GetByID(id int) *Lock
	Insert(lockToCreate LockBaseData) Lock
	Update(id int, lockData LockBaseData) bool
	UpdateLockStatus(id int, newStatus string) bool
	GetNewLockId() int
	Delete(id int) bool

	GetLatestLockTotemHistoryByLockId(lockId int) *LockTotemHistory
	InsertLockTotemHistory(lockTotemHistoryData IntegrateLockBaseData, lockOriginalStatus string) LockTotemHistory
	SetTotemId(lockId int, totemId int)
	GetAllByTotemId(totemId int) []Lock
}

type Lock struct {
	ID      int `json:"id"`
	totemId *int
	*LockBaseData
}

func (l Lock) GetId() int {
	return l.ID
}

type LockBaseData struct {
	Number          int    `json:"numero"`
	Location        string `json:"localizacao"`
	ManufactureYear string `json:"anoDeFabricacao"`
	Model           string `json:"modelo"`
	Status          string `json:"status"`
}

type AssociateBikeBody struct {
	Bike int `json:"bicicleta"`
}

type LockTotemHistory struct {
	ID int `json:"id"`
	IntegrateLockBaseData
	OriginalStatus string `json:"statusOriginal"`
	CreatedAt      string `json:"dataCriacao"`
}

func (l LockTotemHistory) GetId() int {
	return l.ID
}

type IntegrateLockBaseData struct {
	TotemId    int `json:"idTotem"`
	LockId     int `json:"idTranca"`
	EmployeeId int `json:"idFuncionario"`
}

type ReleaseLockBaseData struct {
	TotemId    int    `json:"idTotem"`
	LockId     int    `json:"idTranca"`
	EmployeeId int    `json:"idFuncionario"`
	NewStatus  string `json:"statusAcaoReparador"`
}

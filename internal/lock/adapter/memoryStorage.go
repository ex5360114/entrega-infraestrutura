package adapter

import (
	"entrega-infraestrutura/internal/memoryStorage"
	"time"
)

var totemId1 = 1

// Executed once, at program startup
var sharedAdapterAddress = &memoryStorageAdapter{locks: []*Lock{
	{
		ID: 1,
		LockBaseData: &LockBaseData{
			Location:        "Rio de Janeiro",
			Number:          12345,
			ManufactureYear: "2020",
			Model:           "Caloi",
			Status:          "OCUPADA",
		},
		totemId: &totemId1,
	},
	{
		ID: 2,
		LockBaseData: &LockBaseData{
			Location:        "Rio de Janeiro",
			Number:          12345,
			ManufactureYear: "2020",
			Model:           "Caloi",
			Status:          "DISPONIVEL",
		},
		totemId: &totemId1,
	},
	{
		ID: 3,
		LockBaseData: &LockBaseData{
			Location:        "Rio de Janeiro",
			Number:          12345,
			ManufactureYear: "2020",
			Model:           "Caloi",
			Status:          "OCUPADA",
		},
		totemId: &totemId1,
	},
	{
		ID: 4,
		LockBaseData: &LockBaseData{
			Location:        "Rio de Janeiro",
			Number:          12345,
			ManufactureYear: "2020",
			Model:           "Caloi",
			Status:          "OCUPADA",
		},
		totemId: &totemId1,
	},
	{
		ID: 5,
		LockBaseData: &LockBaseData{
			Location:        "Rio de Janeiro",
			Number:          12345,
			ManufactureYear: "2020",
			Model:           "Caloi",
			Status:          "EM_REPARO",
		},
		totemId: &totemId1,
	},
	{
		ID: 6,
		LockBaseData: &LockBaseData{
			Location:        "Rio de Janeiro",
			Number:          12345,
			ManufactureYear: "2020",
			Model:           "Caloi",
			Status:          "REPARO_SOLICITADO",
		},
		totemId: &totemId1,
	},
}, lockTotemHistory: []LockTotemHistory{
	{
		ID: 1,
		IntegrateLockBaseData: IntegrateLockBaseData{
			TotemId:    1,
			LockId:     5,
			EmployeeId: 1,
		},
		OriginalStatus: "NOVA",
		CreatedAt:      time.Now().Format(time.DateTime),
	},
}}

func NewMemoryStorage() Adapter {
	memoryStorage := sharedAdapterAddress
	return memoryStorage
}

type memoryStorageAdapter struct {
	locks            []*Lock
	lockTotemHistory []LockTotemHistory
}

func (l *memoryStorageAdapter) Get() []Lock {
	locks := []Lock{}
	for _, lockPtr := range l.locks {
		locks = append(locks, *lockPtr)
	}
	return locks
}

func (l *memoryStorageAdapter) GetByID(id int) *Lock {
	lock := memoryStorage.GetById(&l.locks, id)
	if lock == nil {
		return nil
	}
	return *lock
}

func (l *memoryStorageAdapter) Insert(lockToCreate LockBaseData) Lock {
	var lock = Lock{
		ID:           l.GetNewLockId(),
		LockBaseData: &lockToCreate,
	}

	memoryStorage.Insert(&l.locks, &lock)
	insertedLock := l.GetByID(lock.ID)
	return *insertedLock
}

func (l *memoryStorageAdapter) Update(id int, lockData LockBaseData) bool {
	lockToBeUpdated := l.GetByID(id)

	if lockToBeUpdated == nil {
		return false
	}

	*lockToBeUpdated.LockBaseData = lockData
	return true
}

func (l *memoryStorageAdapter) UpdateLockStatus(id int, newStatus string) bool {
	lockToBeUpdated := l.GetByID(id)
	if lockToBeUpdated == nil {
		return false
	}

	lockToBeUpdated.Status = newStatus
	return true
}

func (l *memoryStorageAdapter) GetNewLockId() int {
	highestId := 0
	for _, lock := range l.locks {
		if lock.ID > highestId {
			highestId = lock.ID
		}
	}
	return highestId + 1
}

func (l *memoryStorageAdapter) Delete(id int) bool {
	return memoryStorage.Delete(&l.locks, id)
}

func (l *memoryStorageAdapter) GetLatestLockTotemHistoryByLockId(lockId int) *LockTotemHistory {
	latestLockTotemPtr := &LockTotemHistory{}
	for _, lockTotemHistory := range l.lockTotemHistory {
		// Ignoring these would be bad if it was a frontend input.
		latestDate, _ := time.Parse(time.DateTime, latestLockTotemPtr.CreatedAt)
		currentItemDate, _ := time.Parse(time.DateTime, lockTotemHistory.CreatedAt)

		if lockTotemHistory.LockId == lockId && latestDate.Before(currentItemDate) {
			latestLockTotemPtr = &lockTotemHistory
		}
	}

	return latestLockTotemPtr
}

func (l *memoryStorageAdapter) InsertLockTotemHistory(lockTotemHistoryData IntegrateLockBaseData, lockOriginalStatus string) LockTotemHistory {
	newId := l.getNewLockTotemHistoryId()
	lockTotemHistory := LockTotemHistory{
		ID:                    newId,
		IntegrateLockBaseData: lockTotemHistoryData,
		OriginalStatus:        lockOriginalStatus,
		CreatedAt:             time.Now().Format(time.DateTime),
	}

	memoryStorage.Insert(&l.lockTotemHistory, lockTotemHistory)
	return lockTotemHistory
}

func (l *memoryStorageAdapter) getNewLockTotemHistoryId() int {
	highestId := 0
	for _, lockTotemHistory := range l.lockTotemHistory {
		if lockTotemHistory.ID > highestId {
			highestId = lockTotemHistory.ID
		}
	}
	return highestId + 1
}

func (l *memoryStorageAdapter) SetTotemId(lockId int, totemId int) {
	lock := l.GetByID(lockId)
	if lock == nil {
		return
	}

	lock.totemId = &totemId
}

func (b *memoryStorageAdapter) GetAllByTotemId(totemId int) []Lock {
	list := []Lock{}
	for _, lock := range b.locks {
		if lock.totemId != nil && *lock.totemId == totemId {
			list = append(list, *lock)
		}
	}
	return list
}

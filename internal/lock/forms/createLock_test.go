package forms

import (
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"errors"
	"testing"
)

func TestValidateLockCreation(t *testing.T) {
	tests := []struct {
		data           lockAdapter.LockBaseData
		expectedOutput []error
	}{
		{
			lockAdapter.LockBaseData{Model: "", Location: "", ManufactureYear: "", Status: "", Number: 1},
			[]error{
				errors.New("status: invalid lock status"),
				errors.New("anoDeFabricacao: string is empty"),
				errors.New("localizacao: string is empty"),
				errors.New("modelo: string is empty"),
			},
		},
		{
			lockAdapter.LockBaseData{Model: "exampleModel", Location: "exampleLocation", ManufactureYear: "exampleYear", Status: "NOVA", Number: 1},
			[]error{},
		},
	}

	for _, test := range tests {
		output := ValidateLockCreation(test.data)
		if len(output) != len(test.expectedOutput) {
			t.Errorf("Expected output to have length %d, but got %d", len(test.expectedOutput), len(output))
		}

		for i, err := range output {
			if err.Error() != test.expectedOutput[i].Error() {
				t.Errorf("Expected error to be '%v', but got '%v'", test.expectedOutput[i], err)
			}
		}
	}
}

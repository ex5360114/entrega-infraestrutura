package forms

import (
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"errors"
	"testing"
)

func TestValidateLockRelease(t *testing.T) {
	tests := []struct {
		data           lockAdapter.ReleaseLockBaseData
		expectedOutput []error
	}{
		{
			lockAdapter.ReleaseLockBaseData{TotemId: 0, LockId: 0, EmployeeId: 0, NewStatus: ""},
			[]error{
				errors.New("idFuncionario: number must be greater than zero"),
				errors.New("idTotem: number must be greater than zero"),
				errors.New("idTranca: number must be greater than zero"),
				errors.New("statusAcaoReparador: invalid lock status"),
			},
		},
		{
			lockAdapter.ReleaseLockBaseData{TotemId: 1, LockId: 1, EmployeeId: 1, NewStatus: "APOSENTADA"},
			[]error{},
		},
		{
			lockAdapter.ReleaseLockBaseData{TotemId: 1, LockId: 1, EmployeeId: 1, NewStatus: "EM_REPARO"},
			[]error{},
		},
	}

	for _, test := range tests {
		output := ValidateLockRelease(test.data)
		if len(output) != len(test.expectedOutput) {
			t.Errorf("Expected output to have length %d, but got %d", len(test.expectedOutput), len(output))
		}
	}
}

package forms

import (
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"entrega-infraestrutura/internal/validations"
)

func ValidateLockCreation(data lockAdapter.LockBaseData) []error {
	number := validations.FieldValidation{
		FieldName:  "numero",
		FieldValue: data.Number,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	model := validations.FieldValidation{
		FieldName:  "modelo",
		FieldValue: data.Model,
		Validations: []validations.ValidationFunc{
			validations.IsEmptyString,
		},
	}

	location := validations.FieldValidation{
		FieldName:  "localizacao",
		FieldValue: data.Location,
		Validations: []validations.ValidationFunc{
			validations.IsEmptyString,
		},
	}

	year := validations.FieldValidation{
		FieldName:  "anoDeFabricacao",
		FieldValue: data.Status,
		Validations: []validations.ValidationFunc{
			validations.IsEmptyString,
		},
	}

	status := validations.FieldValidation{
		FieldName:  "status",
		FieldValue: data.Status,
		Validations: []validations.ValidationFunc{
			validations.NewLockStatusCreation,
		},
	}

	validationsList := []validations.FieldValidation{
		status, year, location, model, number,
	}

	validationsErr := validations.Validator(validationsList)
	return validationsErr
}

func ValidateLockCreationMock(data lockAdapter.LockBaseData) []error {
	return make([]error, 0)
}

package forms

import (
	"errors"
	"strings"
)

// Different form, used to validate a status received as part of the URL path
func ValidateLockStatus(status string) (string, []error) {
	validationErrors := []error{}

	formattedStatus := strings.ToLower(status)
	if formattedStatus != "destrancar" && formattedStatus != "trancar" {
		validationErrors = append(validationErrors, errors.New("status invalido"))
	}

	return formattedStatus, validationErrors
}

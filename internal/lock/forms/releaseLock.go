package forms

import (
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"entrega-infraestrutura/internal/validations"
)

func ValidateLockRelease(data lockAdapter.ReleaseLockBaseData) []error {
	totemId := validations.FieldValidation{
		FieldName:  "idTotem",
		FieldValue: data.TotemId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	employeeId := validations.FieldValidation{
		FieldName:  "idTotem",
		FieldValue: data.TotemId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	lockId := validations.FieldValidation{
		FieldName:  "idTranca",
		FieldValue: data.LockId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	status := validations.FieldValidation{
		FieldName:  "statusAcaoReparador",
		FieldValue: data.NewStatus,
		Validations: []validations.ValidationFunc{
			validations.LockReleaseStatus,
		},
	}

	validationsList := []validations.FieldValidation{
		totemId, employeeId, lockId, status,
	}

	validationsErr := validations.Validator(validationsList)
	return validationsErr
}

func ValidateLockReleaseMock(data lockAdapter.ReleaseLockBaseData) []error {
	return make([]error, 0)
}

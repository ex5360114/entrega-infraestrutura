package forms

import (
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"errors"
	"testing"
)

func TestValidateLockIntegration(t *testing.T) {
	tests := []struct {
		data           lockAdapter.IntegrateLockBaseData
		expectedOutput []error
	}{
		{
			lockAdapter.IntegrateLockBaseData{TotemId: 0, LockId: 0, EmployeeId: 0},
			[]error{
				errors.New("idFuncionario: number must be greater than zero"),
				errors.New("idTotem: number must be greater than zero"),
				errors.New("idTranca: number must be greater than zero"),
			},
		},
		{
			lockAdapter.IntegrateLockBaseData{TotemId: 1, LockId: 1, EmployeeId: 1},
			[]error{},
		},
	}

	for _, test := range tests {
		output := ValidateLockIntegration(test.data)
		if len(output) != len(test.expectedOutput) {
			t.Errorf("Expected output to have length %d, but got %d", len(test.expectedOutput), len(output))
		}
	}
}

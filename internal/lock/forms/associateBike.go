package forms

import (
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"entrega-infraestrutura/internal/validations"
)

func ValidateBikeAssociation(data lockAdapter.AssociateBikeBody) []error {
	status := validations.FieldValidation{
		FieldName:  "bicicleta",
		FieldValue: data.Bike,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	validationsList := []validations.FieldValidation{
		status,
	}

	validationsErr := validations.Validator(validationsList)
	return validationsErr
}

func ValidateBikeAssociationMock(data lockAdapter.LockBaseData) []error {
	return make([]error, 0)
}

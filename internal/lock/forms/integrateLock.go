package forms

import (
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"entrega-infraestrutura/internal/validations"
)

func ValidateLockIntegration(data lockAdapter.IntegrateLockBaseData) []error {
	totemId := validations.FieldValidation{
		FieldName:  "idTotem",
		FieldValue: data.TotemId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	employeeId := validations.FieldValidation{
		FieldName:  "idTotem",
		FieldValue: data.TotemId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	lockId := validations.FieldValidation{
		FieldName:  "idTranca",
		FieldValue: data.LockId,
		Validations: []validations.ValidationFunc{
			validations.MustBeGreaterThanZero,
		},
	}

	validationsList := []validations.FieldValidation{
		totemId, employeeId, lockId,
	}

	validationsErr := validations.Validator(validationsList)
	return validationsErr
}
func ValidateLockIntegrationMock(data lockAdapter.IntegrateLockBaseData) []error {
	return make([]error, 0)
}

package lock

import (
	"entrega-infraestrutura/internal/apiError"
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	"entrega-infraestrutura/internal/external"
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"entrega-infraestrutura/internal/rental"
	totemAdapter "entrega-infraestrutura/internal/totem/adapter"
	"errors"
	"fmt"
	"net/http"
)

type LockDomain interface {
	Get() ([]lockAdapter.Lock, *apiError.ApiError)
	GetByID(id int) (*lockAdapter.Lock, *apiError.ApiError)
	Insert(lockData lockAdapter.LockBaseData) (lockAdapter.Lock, *apiError.ApiError)
	Update(id int, lockData lockAdapter.LockBaseData) (lockAdapter.Lock, *apiError.ApiError)
	Delete(id int) *apiError.ApiError
	IncludeLockOnTotem(data lockAdapter.IntegrateLockBaseData) *apiError.ApiError
	ReleaseLockFromTotem(data lockAdapter.ReleaseLockBaseData) *apiError.ApiError
	UpdateLockStatus(id int, bikeId int, newStatus string) (lockAdapter.Lock, *apiError.ApiError)
	GetBikeByLockId(id int) (bikeAdapter.Bike, *apiError.ApiError)
}

type lockDomain struct {
	lockAdapter  lockAdapter.Adapter
	totemAdapter totemAdapter.TotemAdapter
	bikeAdapter  bikeAdapter.BikeAdapter

	rentalService   rental.RentalService
	externalService external.ExternalService
}

func NewInMemory() LockDomain {
	lockAdapter := lockAdapter.NewMemoryStorage()
	totemAdapter := totemAdapter.NewMemoryStorage()
	bikeAdapter := bikeAdapter.NewMemoryStorage()

	// Update these later on integration phase
	rentalService := rental.MakeMockRentalService()
	externalService := external.MakeMockExternalService()

	return &lockDomain{
		lockAdapter:     lockAdapter,
		totemAdapter:    totemAdapter,
		bikeAdapter:     bikeAdapter,
		rentalService:   rentalService,
		externalService: externalService,
	}
}

func (l *lockDomain) Insert(lockData lockAdapter.LockBaseData) (lockAdapter.Lock, *apiError.ApiError) {
	lock := l.lockAdapter.Insert(lockData)
	return lock, nil
}

func (l *lockDomain) Get() ([]lockAdapter.Lock, *apiError.ApiError) {
	locks := l.lockAdapter.Get()
	return locks, nil
}

func (l *lockDomain) GetByID(id int) (*lockAdapter.Lock, *apiError.ApiError) {
	locks := l.lockAdapter.GetByID(id)
	if locks == nil {
		return nil, apiError.AssetNotFoundApiError()
	}

	return locks, nil
}

func (l *lockDomain) Update(id int, lockData lockAdapter.LockBaseData) (lockAdapter.Lock, *apiError.ApiError) {
	lockBeforeUpdate, apiErr := l.GetByID(id)
	if apiErr != nil {
		return lockAdapter.Lock{}, apiErr
	}

	if lockBeforeUpdate.Number != lockData.Number {
		return lockAdapter.Lock{}, apiError.New(errors.New("o numero da tranca nao pode ser editado"), 400)
	}

	result := l.lockAdapter.Update(id, lockData)
	if !result {
		return lockAdapter.Lock{}, apiError.InternalServerErrorApiError(errors.New("não foi possível atualizar a tranca"))
	}

	lock, apiErr := l.GetByID(id)
	if apiErr != nil {
		return lockAdapter.Lock{}, apiErr
	}

	return *lock, nil
}

func (l *lockDomain) Delete(id int) *apiError.ApiError {
	_, apiErr := l.GetByID(id)
	if apiErr != nil {
		return apiErr
	}

	result := l.lockAdapter.Delete(id)

	if !result {
		return apiError.InternalServerErrorApiError(errors.New("não foi possível deletar a tranca"))
	}

	return nil

}

func (l *lockDomain) IncludeLockOnTotem(data lockAdapter.IntegrateLockBaseData) *apiError.ApiError {
	lock := l.lockAdapter.GetByID(data.LockId)
	if lock == nil {
		return apiError.AssetNotFoundApiError()
	}

	if lock.Status == "EM_REPARO" {
		latestLockTotemHistory := l.lockAdapter.GetLatestLockTotemHistoryByLockId(lock.ID)
		if latestLockTotemHistory == nil {
			return apiError.InternalServerErrorApiError(errors.New("uma tranca em reparo deveria ter um registro associado ao reparo"))
		}

		if latestLockTotemHistory.EmployeeId != data.EmployeeId {
			return apiError.New(errors.New("apenas o funcionario que realizou o reparo pode integrar a tranca novamente"), http.StatusBadRequest)
		}
	}

	totem := l.totemAdapter.GetByID(data.TotemId)
	if totem == nil {
		return apiError.AssetNotFoundApiError()
	}

	employee, err := l.rentalService.GetEmployeeById(data.EmployeeId)
	if err != nil {
		return apiError.InternalServerErrorApiError(errors.New("nao foi possivel encontrar informacoes sobre funcionario"))
	}

	historyRow := l.lockAdapter.InsertLockTotemHistory(data, lock.Status)
	l.lockAdapter.UpdateLockStatus(lock.ID, "DISPONIVEL")
	l.lockAdapter.SetTotemId(lock.ID, data.TotemId)

	err = l.externalService.SendEmail(external.SendEmailParams{
		Email:   employee.Email,
		Subject: "Inclusao de tranca",
		Message: fmt.Sprintf("Voce incluiu a tranca de ID %v no totem de ID %v no dia %v", lock.ID, totem.ID, historyRow.CreatedAt),
	})

	if err != nil {
		return apiError.InternalServerErrorApiError(errors.New("nao foi possivel enviar email para funcionario"))
	}

	return nil
}

func (l *lockDomain) ReleaseLockFromTotem(data lockAdapter.ReleaseLockBaseData) *apiError.ApiError {
	lock := l.lockAdapter.GetByID(data.LockId)
	if lock == nil {
		return apiError.AssetNotFoundApiError()
	}

	totem := l.totemAdapter.GetByID(data.TotemId)
	if totem == nil {
		return apiError.AssetNotFoundApiError()
	}

	employee, err := l.rentalService.GetEmployeeById(data.EmployeeId)
	if err != nil {
		return apiError.InternalServerErrorApiError(errors.New("nao foi possivel encontrar informacoes sobre funcionario"))
	}

	historyRow := l.lockAdapter.InsertLockTotemHistory(lockAdapter.IntegrateLockBaseData{
		TotemId:    data.LockId,
		LockId:     data.LockId,
		EmployeeId: data.EmployeeId,
	}, lock.Status)

	l.lockAdapter.UpdateLockStatus(lock.ID, data.NewStatus)
	l.lockAdapter.SetTotemId(lock.ID, 0)

	err = l.externalService.SendEmail(external.SendEmailParams{
		Email:   employee.Email,
		Subject: "Retirada de tranca",
		Message: fmt.Sprintf("Voce retirou a tranca de ID %v do totem de ID %v no dia %v", lock.ID, totem.ID, historyRow.CreatedAt),
	})

	if err != nil {
		return apiError.InternalServerErrorApiError(errors.New("nao foi possivel enviar email para funcionario"))
	}

	return nil
}

func (l *lockDomain) UpdateLockStatus(id int, bikeId int, action string) (lockAdapter.Lock, *apiError.ApiError) {
	currentLock, getByIdError := l.GetByID(id)
	if getByIdError != nil {
		return lockAdapter.Lock{}, getByIdError
	}

	if currentLock == nil {
		return lockAdapter.Lock{}, apiError.AssetNotFoundApiError()
	}

	if bikeId != 0 {
		bike := l.bikeAdapter.GetByID(bikeId)
		if bike == nil {
			return lockAdapter.Lock{}, apiError.AssetNotFoundApiError()
		}
	}

	if action == "trancar" {
		l.lockAdapter.UpdateLockStatus(id, "OCUPADA")
		if bikeId != 0 {
			l.bikeAdapter.SetLockId(bikeId, id)
			currentBike := l.bikeAdapter.GetByID(bikeId)
			currentBike.BikeBaseData.Status = "DISPONIVEL"
			l.bikeAdapter.Update(bikeId, *currentBike.BikeBaseData)
		}

	} else {
		l.lockAdapter.UpdateLockStatus(id, "LIVRE")
		if bikeId != 0 {
			l.bikeAdapter.SetLockId(bikeId, id)
			currentBike := l.bikeAdapter.GetByID(bikeId)
			currentBike.BikeBaseData.Status = "EM_USO"
			l.bikeAdapter.Update(bikeId, *currentBike.BikeBaseData)
		}
	}

	updatedLock, getByIdError := l.GetByID(id)
	if getByIdError != nil {
		return lockAdapter.Lock{}, getByIdError
	}

	return *updatedLock, nil
}

func (l *lockDomain) GetBikeByLockId(id int) (bikeAdapter.Bike, *apiError.ApiError) {
	bike := l.bikeAdapter.GetByLockId(id)
	if bike.ID == 0 {
		return bike, apiError.AssetNotFoundApiError()
	}

	return bike, nil
}

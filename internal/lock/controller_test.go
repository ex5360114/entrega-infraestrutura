package lock

import (
	"bytes"
	"encoding/json"
	"entrega-infraestrutura/internal/apiError"
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"entrega-infraestrutura/internal/lock/forms"
	"entrega-infraestrutura/internal/utils"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func makeTestController() LockController {
	return LockController{
		Mux:                          http.NewServeMux(),
		DefaultHandlerCreator:        utils.MakeHTTPHandlerFuncMock,
		HandlerWithValidationCreator: utils.MakeHTTPHandlerWithValidationFuncMock,

		LockDomain: makeMockDomain(),

		IdValidator: utils.MockValidateId,

		// Only physical dependencies, I need a way to write JSON to verify the controller output, otherwise the test doesnt make sense.
		BodyDecoder: utils.DecodeBody,
		JsonWriter:  utils.WriteJson,

		LockCreationValidation:    forms.ValidateLockCreationMock,
		LockIntegrationValidation: forms.ValidateLockIntegrationMock,
		LockReleaseValidation:     forms.ValidateLockReleaseMock,

		InternalServerErrorCreator: apiError.InternalServerErrorApiMock,
		ValidationErrorsCreator:    apiError.NewWithValidationErrors,
	}
}

func TestGetLockHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFunc(controller.getLockHTTPHandler))

	req, err := http.NewRequest(http.MethodGet, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(lockAdapter.Lock{ID: 1, LockBaseData: &lockAdapter.LockBaseData{
		Number:          1,
		Location:        "Location mock",
		ManufactureYear: "2000",
		Model:           "Model mock",
		Status:          "NOVA",
	}})
	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected lock object as response body, got: %v", string(body))
	}
}

func TestGetLocksHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(controller.DefaultHandlerCreator(controller.getLocksHTTPHandler))

	req, err := http.NewRequest(http.MethodGet, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(make([]lockAdapter.Adapter, 0))

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected empty lock array as response body, got: %v", string(body))
	}
}

func TestInsertLockHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.insertLockHTTPHandler))

	bodyPayload := lockAdapter.LockBaseData{
		Number:          1,
		Location:        "Location mock",
		ManufactureYear: "2000",
		Model:           "Model mock",
		Status:          "NOVA",
	}
	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(lockAdapter.Lock{ID: 1, LockBaseData: &lockAdapter.LockBaseData{
		Number:          1,
		Location:        "Location mock",
		ManufactureYear: "2000",
		Model:           "Model mock",
		Status:          "NOVA",
	}})

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected lock as response body, got: %v", string(body))
	}
}

func TestUpdateLockHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.insertLockHTTPHandler))

	bodyPayload := lockAdapter.LockBaseData{
		Number:          1,
		Location:        "Location mock",
		ManufactureYear: "2000",
		Model:           "Model mock",
		Status:          "NOVA",
	}
	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(lockAdapter.Lock{ID: 1, LockBaseData: &lockAdapter.LockBaseData{
		Number:          1,
		Location:        "Location mock",
		ManufactureYear: "2000",
		Model:           "Model mock",
		Status:          "NOVA",
	}})

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected lock as response body, got: %v", string(body))
	}
}

func TestDeleteLockHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(controller.DefaultHandlerCreator(controller.deleteLockHTTPHandler))

	req, err := http.NewRequest(http.MethodDelete, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()
}

func TestReleaseLockHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.releaseLockHTTPHandler))

	bodyPayload := lockAdapter.ReleaseLockBaseData{
		TotemId:    1,
		LockId:     1,
		EmployeeId: 1,
		NewStatus:  "EM_REPARO",
	}

	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	want, err := json.Marshal(make(map[string]string))

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected null as response body, got: %v", string(body))
	}
}

func TestIntegrateLockHandler(t *testing.T) {
	controller := makeTestController()
	s := httptest.NewServer(utils.MakeHTTPHandlerFuncWithValidation(controller.integrateLockHTTPHandler))

	bodyPayload := lockAdapter.IntegrateLockBaseData{
		TotemId:    1,
		LockId:     1,
		EmployeeId: 1,
	}

	bodyBytes, err := json.Marshal(bodyPayload)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, s.URL, bytes.NewReader(bodyBytes))
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	want, err := json.Marshal(make(map[string]string))

	if err != nil {
		t.Fatal(err)
	}

	if string(body) != string(want)+"\n" {
		t.Errorf("Expected null as response body, got: %v", string(body))
	}
}

func TestInitializeRoutes(t *testing.T) {
	controller := makeTestController()
	controller.InitializeRoutes()
}

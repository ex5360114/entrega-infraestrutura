package lock

import (
	"entrega-infraestrutura/internal/apiError"
	bikeAdapter "entrega-infraestrutura/internal/bike/adapter"
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
)

type mockDomain struct{}

func (t *mockDomain) Insert(lockData lockAdapter.LockBaseData) (lockAdapter.Lock, *apiError.ApiError) {
	return lockAdapter.Lock{ID: 1, LockBaseData: &lockData}, nil
}

func (t *mockDomain) Get() ([]lockAdapter.Lock, *apiError.ApiError) {
	return make([]lockAdapter.Lock, 0), nil
}

func (t *mockDomain) GetByID(id int) (*lockAdapter.Lock, *apiError.ApiError) {
	return &lockAdapter.Lock{ID: 1, LockBaseData: &lockAdapter.LockBaseData{
		Number:          1,
		Location:        "Location mock",
		ManufactureYear: "2000",
		Model:           "Model mock",
		Status:          "NOVA",
	}}, nil
}

func (t *mockDomain) Update(id int, lockData lockAdapter.LockBaseData) (lockAdapter.Lock, *apiError.ApiError) {
	return lockAdapter.Lock{ID: 1, LockBaseData: &lockData}, nil
}

func (t *mockDomain) Delete(id int) *apiError.ApiError {
	return nil
}

func (l *mockDomain) IncludeLockOnTotem(data lockAdapter.IntegrateLockBaseData) *apiError.ApiError {
	return nil
}

func (l *mockDomain) ReleaseLockFromTotem(data lockAdapter.ReleaseLockBaseData) *apiError.ApiError {
	return nil
}

func (l *mockDomain) UpdateLockStatus(id int, bikeId int, newStatus string) (lockAdapter.Lock, *apiError.ApiError) {
	return lockAdapter.Lock{ID: 1, LockBaseData: &lockAdapter.LockBaseData{
		Number:          1,
		Location:        "Location mock",
		ManufactureYear: "2000",
		Model:           "Model mock",
		Status:          "NOVA",
	}}, nil
}

func (l *mockDomain) GetBikeByLockId(id int) (bikeAdapter.Bike, *apiError.ApiError) {
	return bikeAdapter.Bike{}, nil
}

func makeMockDomain() LockDomain {
	return &mockDomain{}
}

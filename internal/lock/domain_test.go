package lock

import (
	"entrega-infraestrutura/internal/external"
	lockAdapter "entrega-infraestrutura/internal/lock/adapter"
	"entrega-infraestrutura/internal/rental"
	totemAdapter "entrega-infraestrutura/internal/totem/adapter"
	"testing"
)

func TestLockDomainInsert(t *testing.T) {
	domain := &lockDomain{lockAdapter: lockAdapter.NewMock()}

	lockData := lockAdapter.LockBaseData{
		Number:          1,
		Location:        "location",
		ManufactureYear: "2020",
		Model:           "model",
		Status:          "Available",
	}

	lock, err := domain.Insert(lockData)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	if lock.ID != 2 {
		t.Errorf("Expected lock ID 2, got %d", lock.ID)
	}

	if lock.Location != lockData.Location {
		t.Errorf("Expected lock to have specific location, got %s", lock.Location)
	}
}

func TestLockDomainGet(t *testing.T) {
	domain := &lockDomain{lockAdapter: lockAdapter.NewMock()}

	locks, err := domain.Get()
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	if len(locks) != 0 {
		t.Errorf("Expected 0 locks, got %d", len(locks))
	}
}

func TestLockDomainUpdate(t *testing.T) {
	domain := &lockDomain{lockAdapter: lockAdapter.NewMock()}

	lockData := lockAdapter.LockBaseData{
		Number:          1,
		Location:        "location",
		ManufactureYear: "2020",
		Model:           "model",
		Status:          "Available",
	}

	lock, err := domain.Update(1, lockData)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	if lockData.Location != lock.Location {
		t.Errorf("Expected lock with specific location, got other location")
	}
}

func TestIncludeLockOnTotem(t *testing.T) {
	domain := &lockDomain{lockAdapter: lockAdapter.NewMock(), totemAdapter: totemAdapter.NewMock(), externalService: external.MakeMockExternalService(), rentalService: rental.MakeMockRentalService()}
	data := lockAdapter.IntegrateLockBaseData{
		TotemId:    1,
		LockId:     1,
		EmployeeId: 1,
	}

	err := domain.IncludeLockOnTotem(data)

	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
}

func TestReleaseLockFromTotem(t *testing.T) {
	domain := &lockDomain{lockAdapter: lockAdapter.NewMock(), totemAdapter: totemAdapter.NewMock(), externalService: external.MakeMockExternalService(), rentalService: rental.MakeMockRentalService()}
	data := lockAdapter.ReleaseLockBaseData{
		TotemId:    1,
		LockId:     1,
		EmployeeId: 1,
		NewStatus:  "APOSENTADA",
	}

	err := domain.ReleaseLockFromTotem(data)

	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
}

func TestLockDomainDelete(t *testing.T) {
	domain := &lockDomain{lockAdapter: lockAdapter.NewMock()}

	err := domain.Delete(1)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
}

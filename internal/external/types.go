package external

type ExternalService interface {
	SendEmail(data SendEmailParams) error
}

type SendEmailParams struct {
	Email   string `json:"email"`
	Subject string `json:"subject"`
	Message string `json:"mensagem"`
}

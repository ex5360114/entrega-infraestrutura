package external

import "testing"

func TestSendEmail(t *testing.T) {
	service := MakeMockExternalService()

	mockParams := SendEmailParams{
		Email:   "example@email",
		Subject: "Example",
		Message: "Example",
	}
	err := service.SendEmail(mockParams)
	if err != nil {
		t.Error("expected mock to not error")
	}
}

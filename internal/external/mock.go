package external

import "net/http"

func MakeMockExternalService() ExternalService {
	return &mockExternalService{}
}

type mockExternalService struct{}

func (e *mockExternalService) SendEmail(data SendEmailParams) error {
	_, err := http.Get("http://134.209.68.180:4040/enviarEmail")
	return err
}

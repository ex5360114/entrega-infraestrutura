# Entidades

### CDU 13 - Manter Cadastro de Tranca
#### Fluxo Principal (criar)

- [X] GET /tranca
- [] POST /tranca 
    - status inicial de tranca é "NOVA"
    - Não pode ser editada
    - Tudo obrigatorio


#### Alternativo1 (editar)

- [X] GET /tranca/{id}
- [] PUT /tranca/{id}

#### Alternativo2 (excluir)

- [X] GET /tranca/{id}
- [X] DELETE /tranca/{id}
    - Apenas trancas sem bicicletas podem ser excluídas

### CDU 13 - Manter Cadastro de Totem

#### Fluxo Principal (criar)

- [X] GET /totem
- [] POST /totem
    - status inicial de tranca é "NOVA"
    - Não pode ser editada
    - Tudo obrigatorio


#### Alternativo1 (editar)

- [X] GET /tranca/{id}
- [] PUT /tranca/{id}

#### Alternativo2 (excluir)

- [X] GET /tranca/{id}
- [X] DELETE /tranca/{id}
    - Apenas trancas sem bicicletas podem ser excluídas

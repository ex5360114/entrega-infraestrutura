# Entrega-Infraestrutura

## Diretórios

Aqui está uma descrição a respeito da estrutura de pastas desse projeto.

Go é uma linguagem que destoa da organização de pastas comuns em projetos de software. Implementar os padrões tradicionais (MVC, Clean Architecture, Arquitetura Hexagonal) em sua forma pura não é tão comum em projetos Go. Por isso, a estrutura de pastas pode variar de projeto para projeto. A [documentação oficial](https://go.dev/doc/modules/layout) possui algumas sugestões para diferentes tipos de projetos, e existem [diversos guias](https://github.com/golang-standards/project-layout/blob/master/README.md) feitos pela comunidade que podem ser consultados. Este projeto se baseou nessas recomendações para a sua estrutura de pastas.

Antes de entrar em detalhes, é importante definir alguns termos que serão utilizados:

- **Projeto** - Tudo que se encontra nesse repositório.

- **Módulo** - Desenvolvedores Go chamam de módulo todo diretório que contém um arquivo `go.mod`. Um módulo é um conjunto de pacotes Go que são versionados juntos. Um módulo pode conter vários pacotes.

- **Pacote** - Um pacote é um conjunto de arquivos `.go` que estão no mesmo diretório e que compartilham o mesmo `package` (declarado na primeira linha de cada arquivo `.go`. Um pacote é a unidade de organização de código em Go.

### `/cmd`

Contém os entrypoints de execução do projeto.

Para esta api, há apenas um subdiretório `/api` com o arquivo `main.go`. Será esse arquivo que irá ser o alvo de build do binário da API (executando na raíz o comando `go build ./cmd/api/main.go`).

Caso tivéssemos outros serviços, como a declaração de um CRON para ser executado em background, ou uma outra API para fins administrativos da aplicação, ou uma outra API para fins de metrificação e analytics, cada um teria seu próprio diretório dentro de `/cmd` com seu próprio arquivo `main.go`, e todos poderiam compartilhar código do diretório `/internal`.

### `/internal`

Contém a maior parte do código da aplicação. É o coração do projeto, no qual estarão dispostos todo o código acessível apenas para as aplicações contidas nesse módulo. Dentro de `/internal`, podem ser aplicados o estilo de arquitetura que melhor se encaixar no projeto. Neste caso, foram utilizados conceitos de Domain Driven Design, com a separação do código em pastas de acordo com o seu respectivo Domínio.

#### `/internal/errors`

Diretório que contém um pacote com funções para tratamento de erros lançados pelos Domínios.

#### `/internal/validations`

Diretório que contém diversas funções de validação individuais

#### `/internal/forms`

Diretório que contém diversas funções de validação de formulário. Utiliza muito de `/internal/validations`.

#### `/internal/utils`

Diretório que contém funções diversas.

#### `/internal/:Domínio`

Diretórios que contém os pacotes de cada Domínio da aplicação. Cada Domínio possui um diretório com o seu nome, e dentro dele, os arquivos que compõem o Domínio. Cada arquivo é responsável por uma parte do Domínio, como por exemplo:

- `domain`: Contém lógica de negócio. Normalmente dependerá de interfaces presentes em outros arquivos/pacotes para executar suas funcionalidades.
- `controller`: Contém lógica para tratar os dados retornados pelos Domínios antes de serem apresentados por um consumidor. Essa API retornará JSON para seus consumidores, então os handlers serão responsáveis por transformar os objetos de domínio em JSON. Outros exemplos de tratamento de dados seriam transformar os dados para serem enviados para um template HTML, ou para serem enviados para um serviço de mensageria, cliente SSH, WebSocket, TCP etc.

A depender da necessidade da aplicação, podem ser criados outros diretórios dentro de `/internal/:Domínio` para melhor organizar o código. 

#### `/internal/:Domínio/adapter`

Essa pasta armazena arquivos que lidam com lógica para persistência de dados. 
Dentro dessa pasta, vários arquivos são armazenados: 

`types.go` possuirá as definições de tipos usados por todo o domínio, além de interfaces que servirão de contratos para as implementações dos adapters deste domínio. Outros arquivos dentro da pasta adapter poderão implementar essa interface de diversas formas.

Por exemplo, dentro de `/internal/lock/adapter/types.go` existe a definição da interface `LockAdapter`, com seus métodos Get(), GetByID(), Insert()... que serão implementados concretamente pelos arquivos `/internal/lock/adapter/memoryStorage.go` e `/internal/lock/adapter/mock.go`. No geral, `memoryStorage.go` conterá a implementação real a ser utilizada na aplicação, dependendo do pacote `memoryStorage` para manipulação dos dados, enquanto `mock.go` conterá uma implementação falsa daquela interface, apenas retornando um objeto de acordo com a interface. Essa implementação falsa é amplamente utilizada em testes unitários da camada de domínio.
 
Poderá utilizar interfaces para abstrair as implementações de persistência. Nesse trabalho, serão realizadas apenas operações em memória, mas normalmente as interfaces podem ser implementadas com lógica de utilização de bancos de dados. 

### `/build`

Contém arquivos de configuração para CI/CD, scripts de build, Dockerfiles, etc.

Nesse repositório, um único script bash foi necessário para cuidar do deploy da aplicação.

Um arquivo .gitlab-ci.yml foi criado para automatizar o deploy da aplicação no GitLab e um arquivo sonar-project.properties foi criado para configurar o SonarCloud, e ambos se encontram na raíz do projeto por conveniência.

### `/pkg`

Não está presente nesta aplicação, porém é comum de existir em módulos Go que são compartilhados entre diferentes projetos (atuando como uma biblioteca para outro projeto). O diretório `/pkg` contém código que pode ser importado por outros módulos. 

## Deploy

Essa API se encontra hospedada num Droplet da Digital Ocean.

O deploy é automático, e, para realizar o deploy da aplicação, foi utilizado o GitLab CI/CD. O arquivo `.gitlab-ci.yml` contém as etapas necessárias para realizar o deploy da aplicação.

Em resumo, o deploy:

1. utiliza a ferramenta `go test` para executar os testes da aplicação
2. utiliza o `SonarCloud` para realizar a análise estática do código
3. Configura o acesso ao Droplet da Digital Ocean através de SSH
4. Executa o script `build_api.sh`, presente no diretório `/build`

O script `build_api.sh` é responsável por:

1. Realizar o build da aplicação
2. Parar o serviço da aplicação, caso esteja rodando
3. Subir o binário da aplicação para a máquina remota através do `rsync`
4. Iniciar o serviço da aplicação em background

Outros microsserviços podem seguir esse mesmo padrão de deploy para a máquina, copiando as variáveis de ambiente desse repositorio e criando um arquivo de `build_api.sh` adequado para o projeto.
